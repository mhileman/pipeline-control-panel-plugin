package org.nrg.ccf.pcp.xapi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.ExecutionGroup;
import org.nrg.ccf.pcp.dto.PcpCondensedStatus;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.dto.PcpProjectPipelineEntity;
import org.nrg.ccf.pcp.dto.PcpStatusEntityT;
import org.nrg.ccf.pcp.dto.PcpSubmitItem;
import org.nrg.ccf.pcp.dto.PcpSummaryStatus;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.exception.PcpUpdateRunningException;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PcpStatusEntityI;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.constants.Scope;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "Pipeline Control Panel API")
public class PcpApi extends AbstractXapiRestController {

	private static final Map<String,Map<String,String>> disabledCache = new HashMap<>();
	private final PcpStatusEntityService _statusEntityService;
	private final PcpStatusUpdateService _pcpService;
	private final ConfigService _configService;
	private final Gson _gson = new Gson();
	private final PcpUtils _pcpUtils;
	private static final String NULL_STR = "NULL";
	private static final String SUMMARY_STATUS =  "_PIPELINE_";
	private static final Logger _logger = LoggerFactory.getLogger(PcpApi.class);

	@Autowired
	protected PcpApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, PcpStatusUpdateService pcpService,
			PcpStatusEntityService statusEntityService, ConfigService configService, PcpUtils pcpUtils) {
		super(userManagementService, roleHolder);
		_statusEntityService = statusEntityService;
		_pcpService = pcpService;
		_configService = configService;
		_pcpUtils = pcpUtils;
	}
	
	
	@ApiOperation(value = "Gets PCP project pipeline status", notes = "Returns PCP project pipeline status.",
			responseContainer = "List", response = PcpSummaryStatus.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/statusSummary"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<PcpSummaryStatus>> getPcpProjectStatus(@PathVariable("projectId") final String projectId,
    			@RequestParam(value="includeSubgroupSummary", defaultValue="false") final Boolean includeSubgroupSummary) throws NrgServiceException {
		final List<PcpStatusEntity> statusEntities = _statusEntityService.getProjectStatus(projectId);
		final List<PcpSummaryStatus> returnList = generateSummaryListFromStatusEntities(statusEntities, includeSubgroupSummary);
		Collections.sort(returnList);
		return new ResponseEntity<>(returnList,HttpStatus.OK);
	}
	

	//@ApiOperation(value = "Gets required PCP pipeline submission parameters", notes = "Gets required PCP pipeline submission status.",
	//		responseContainer = "List", response=PcpSubmitParameter.class)
    //@ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    //@XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/submissionParameters"}, restrictTo=AccessLevel.Read,
    //						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    //@ResponseBody
    //public ResponseEntity<List<PcpSubmitParameter>> getSubmitParameters(@PathVariable("projectId") final String projectId,
    //		@PathVariable("pipelineId") final String pipelineId) throws NrgServiceException {
	//	List<PcpSubmitParameter> returnList;
	//	try {
	//		returnList = _pcpUtils.getRequiredParameters(projectId, pipelineId);
	//		return new ResponseEntity<>(returnList,HttpStatus.OK);
	//	} catch (PcpComponentSetException e) {
	//		_logger.error("PcpComponentException thrown: ", e);
	//		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	//	}
	//}

	@ApiOperation(value = "Gets YAML for submission parameters", notes = "Gets YAML for submission parameters for a pipeline's submitter.",
			responseContainer = "List", response=String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/submitParametersYaml"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> getSubmitParametersYaml(@PathVariable("projectId") final String projectId,
    		@PathVariable("pipelineId") final String pipelineId) throws NrgServiceException {
		List<String> returnList;
		try {
			returnList = _pcpUtils.getSubmitParametersYaml(projectId, pipelineId);
			return new ResponseEntity<>(returnList,HttpStatus.OK);
		} catch (PcpComponentSetException e) {
			_logger.error("PcpComponentException thrown: ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Gets PCP project pipeline status", notes = "Returns PCP project pipeline status.",
			responseContainer = "List", response=PcpCondensedStatusI.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), 
    	@ApiResponse(code = 402, message = "Returned early, continuing to process."),
    	@ApiResponse(code = 500, message = "Unexpected error"),
    	@ApiResponse(code = 409, message = "An update operation is already running for this pipeline.  " +
    			"Please run again later or run a query with cached = true.")
    	})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/status"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<PcpCondensedStatusI>> getPcpProjectStatus(@PathVariable("projectId") final String projectId,
    		@PathVariable("pipelineId") final String pipelineId, 
    		@RequestParam(value="condensed", defaultValue="true") final Boolean condensed,
    		@RequestParam(value="cached", defaultValue="true") final Boolean cached,
    		@RequestParam(value="dontWait", defaultValue="true") final Boolean dontWait,
    		@RequestParam(value="emailForEarlyReturn", defaultValue="false") final Boolean emailForEarlyReturn
    		) throws NrgServiceException {
		boolean earlyReturn = false;
		int seconds = -1;
		try {
			if (!cached || isOnDemandStatusPipeline(projectId,pipelineId)) {
				seconds = _pcpService.updateProjectStatus(projectId, pipelineId, getSessionUser(), dontWait, emailForEarlyReturn);
				earlyReturn = (seconds>0);
			}
		} catch (PcpUpdateRunningException e) {
			_logger.warn("Update process is already running - Skipped this run.(PROJECT=" + projectId + ", PIPELINE=" + pipelineId + ")");
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		} catch (PcpComponentSetException e) {
			_logger.error("Exception thrown updating project status", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			_logger.error("Exception", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		final List<PcpCondensedStatusI> returnList = new ArrayList<>();
		if (condensed) {
			returnList.addAll(_statusEntityService.getProjectPipelineCondensedStatus(projectId, pipelineId));
		} else {
			returnList.addAll(_statusEntityService.getProjectPipelineStatus(projectId, pipelineId));
			
		}
		Collections.sort(returnList);
		if (!earlyReturn) {
			return new ResponseEntity<>(returnList,HttpStatus.OK);
		} else {
			HttpHeaders headers = new HttpHeaders();
			headers.add("estimated-seconds-until-completion", String.valueOf(seconds));
			final ResponseEntity<List<PcpCondensedStatusI>> response = new ResponseEntity<>(returnList,headers,HttpStatus.ACCEPTED);
			return response;
		}
	}

	
	@ApiOperation(value = "Updates selected status entities", notes = "Updates selected status entities.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/updateStatusEntities"},
    						restrictTo=AccessLevel.Member, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> updateStatusEntities(
    		@PathVariable("projectId") final String projectId,
    		@PathVariable("pipelineId") final String pipelineId,
    		@RequestParam(value="dontWait", defaultValue="true") final Boolean dontWait,
    		@RequestParam(value="emailForEarlyReturn", defaultValue="false") final Boolean emailForEarlyReturn,
    		@RequestBody final List<PcpCondensedStatus> entityList) throws NrgServiceException {
		try {
			boolean earlyReturn = false;
			for (final PcpCondensedStatus entity : entityList) {
				if (!entity.getProject().equals(projectId)) {
					return new ResponseEntity<>("ERROR:  One or more StatusEntity records do not belong to the requested project.", HttpStatus.UNPROCESSABLE_ENTITY);
				}
				if (!entity.getPipeline().equals(pipelineId)) {
					return new ResponseEntity<>("ERROR:  One or more StatusEntity records is not for the requested pipeline.", HttpStatus.UNPROCESSABLE_ENTITY);
				}
			} 
			int seconds = 0;
			try {
				List<PcpCondensedStatusI> updateList = new ArrayList<>();
				updateList.addAll(entityList);
				seconds = _pcpService.updateProjectStatus(projectId, pipelineId, getSessionUser(), updateList,
						dontWait, emailForEarlyReturn);
				earlyReturn = (seconds>0);
			} catch (Exception e) {
				_logger.error("Exception", e);
				return new ResponseEntity<>("ERROR:  Process threw an exception - " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			if (!earlyReturn) {
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				HttpHeaders headers = new HttpHeaders();
				headers.add("estimated-seconds-until-completion", String.valueOf(seconds));
				final ResponseEntity<String> response = new ResponseEntity<>(String.valueOf(seconds),headers,HttpStatus.ACCEPTED);
				return response;
			}
		} catch (Exception e) {
			_logger.error("Exception", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@ApiOperation(value = "Gets PCP project pipeline status", notes = "Returns PCP project pipeline status.", response = PcpStatusEntityT.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Status entity returned null."), 
    	@ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/entity/{entity}/group/{group}"},
    						restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<PcpStatusEntityT> getStatusEntity(
    		@PathVariable("projectId") final String projectId,
    		@PathVariable("pipelineId") final String pipelineId, 
    		@PathVariable("entity") final String entity, 
    		@PathVariable("group") final String group, 
    		@RequestParam(value="cached", defaultValue="true") final Boolean cached
    		) throws NrgServiceException {
		try {
			final PcpStatusEntity statusEntity = 
					_statusEntityService.getStatusEntity(projectId, pipelineId, new ExecutionGroup(entity, group));
			if (statusEntity==null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			if (!cached || isOnDemandStatusPipeline(projectId,pipelineId)) {
				_pcpService.refreshStatusEntityCheckAndValidate(statusEntity, getSessionUser());
			}
			return new ResponseEntity<>(statusEntity.getTransferEntity(),HttpStatus.OK);
		} catch (Exception e) {
			_logger.error("Exception", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@ApiOperation(value = "Updates PCP status entities", notes = "Updates PCP status entities.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/updateStatusEntity"},
    						restrictTo=AccessLevel.Member, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> putStatusEntity(
    		@PathVariable("projectId") final String projectId,
    		@PathVariable("pipelineId") final String pipelineId,
    		@RequestBody final PcpStatusEntityT statusEntityT) throws NrgServiceException {
		try {
			final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(statusEntityT.getProject(), statusEntityT.getPipeline(), 
					new ExecutionGroup(statusEntityT.getEntityId(),statusEntityT.getSubGroup()));
			if (statusEntity==null) {
				return new ResponseEntity<>("ERROR:  StatusEntity not found.  This service can be used only to update existing " +
						"status entities.  It cannot create new ones.",HttpStatus.UNPROCESSABLE_ENTITY);
			} else if (!statusEntity.getProject().equals(projectId)) {
				return new ResponseEntity<>("ERROR:  StatusEntity does not belong to the requested project", HttpStatus.UNPROCESSABLE_ENTITY);
			} else if (!statusEntity.getPipeline().equals(pipelineId)) {
				return new ResponseEntity<>("ERROR:  StatusEntity is not for the requested pipeline", HttpStatus.UNPROCESSABLE_ENTITY);
			} else {
				updateStatusEntityFromTransferStatus(statusEntity, statusEntityT);
				_statusEntityService.update(statusEntity);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			_logger.error("Exception", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation(value = "Sets entity values.", 
			notes = "Sets status entity values.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/entity/{entity}/group/{group}/setValues"},
    						restrictTo=AccessLevel.Member, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> setStatusRunning(
    		@PathVariable("projectId") final String projectId,
    		@PathVariable("pipelineId") final String pipelineId,
    		@PathVariable("entity") final String entity, 
    		@PathVariable("group") final String group,
    		@RequestParam(value="status", required=false) final String status,
    		@RequestParam(value="statusInfo", required=false) final String statusInfo,
    		@RequestParam(value="prereqs", required=false) final Boolean prereqs,
    		@RequestParam(value="prereqsInfo", required=false) final String prereqsInfo,
    		@RequestParam(value="validated", required=false) final Boolean validated,
    		@RequestParam(value="validatedInfo", required=false) final String validatedInfo,
    		@RequestParam(value="issues", required=false) final Boolean issues,
    		@RequestParam(value="issuesInfo", required=false) final String issuesInfo,
    		@RequestParam(value="impeded", required=false) final Boolean impeded,
    		@RequestParam(value="impededInfo", required=false) final String impededInfo,
    		@RequestParam(value="notes", required=false) final String notes
    		) throws NrgServiceException {
		try {
			final PcpStatusEntity statusEntity = 
					_statusEntityService.getStatusEntity(projectId, pipelineId, new ExecutionGroup(entity, group));
			if (statusEntity==null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			final Date changeDate = new Date();
			if (status != null) {
				if (!PcpConstants.statusList.contains(status)) {
					return new ResponseEntity<>("Invalid status value.  Valid values are:  " +
							PcpConstants.statusList.toString(), HttpStatus.BAD_REQUEST);
				}
				if (!status.equalsIgnoreCase(NULL_STR )) {
					statusEntity.setStatus(status);
				} else {
					statusEntity.setStatus("");
				}
				statusEntity.setStatusTime(changeDate);
			}
			if (statusInfo != null) {
				if (!statusInfo.equalsIgnoreCase(NULL_STR )) {
					statusEntity.setStatusInfo(statusInfo);
				} else {
					statusEntity.setStatusInfo("");
				}
				statusEntity.setStatusTime(changeDate);
			}
			if (prereqs != null) {
				statusEntity.setPrereqs(prereqs);
				statusEntity.setPrereqsTime(changeDate);
			}
			if (prereqsInfo != null) {
				if (!prereqsInfo.equalsIgnoreCase(NULL_STR )) {
					statusEntity.setPrereqsInfo(prereqsInfo);
				} else {
					statusEntity.setPrereqsInfo("");
				}
				statusEntity.setPrereqsTime(changeDate);
			}
			if (validated != null) {
				statusEntity.setValidated(validated);
				statusEntity.setValidatedTime(changeDate);
			}
			if (validatedInfo != null) {
				if (!validatedInfo.equalsIgnoreCase(NULL_STR )) {
					statusEntity.setValidatedInfo(validatedInfo);
				} else {
					statusEntity.setValidatedInfo("");
				}
				statusEntity.setValidatedTime(changeDate);
			}
			if (issues != null) {
				statusEntity.setIssues(issues);
				statusEntity.setIssuesTime(changeDate);
			}
			if (issuesInfo != null) {
				if (!issuesInfo.equalsIgnoreCase(NULL_STR )) {
					statusEntity.setIssuesInfo(issuesInfo);
				} else {
					statusEntity.setIssuesInfo("");
				}
				statusEntity.setIssuesTime(changeDate);
			}
			if (impeded != null) {
				statusEntity.setImpeded(impeded);
				statusEntity.setImpededTime(changeDate);
			}
			if (impededInfo != null) {
				if (!impededInfo.equalsIgnoreCase(NULL_STR )) {
					statusEntity.setImpededInfo(impededInfo);
				} else {
					statusEntity.setImpededInfo("");
				}
				statusEntity.setImpededTime(changeDate);
			}
			if (notes != null) {
				if (!notes.equalsIgnoreCase(NULL_STR )) {
					statusEntity.setNotes(notes);
				} else {
					statusEntity.setNotes("");
				}
			}
			_statusEntityService.update(statusEntity);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			_logger.error("Exception", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	@ApiOperation(value = "Submit PCP pipeline jobs", notes = "Submit PCP pipeline jobs.", response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/pipelineControlPanel/project/{projectId}/pipeline/{pipelineId}/pipelineSubmit"},
    						restrictTo=AccessLevel.Member, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> submitJobs(
    		@PathVariable("projectId") final String projectId,
    		@PathVariable("pipelineId") final String pipelineId,
    		@RequestParam(value="forceProcessLocally", defaultValue="false") final Boolean forceProcessLocally,
    		@RequestBody final PcpSubmitItem submitItem) throws NrgServiceException {
		try {
			final List<PcpCondensedStatus> entityList = submitItem.getEntities();
			for (final PcpCondensedStatus entity : entityList) {
				if (!entity.getProject().equals(projectId)) {
					return new ResponseEntity<>("ERROR:  One or more StatusEntity records do not belong to the requested project.", HttpStatus.UNPROCESSABLE_ENTITY);
				}
				if (!entity.getPipeline().equals(pipelineId)) {
					return new ResponseEntity<>("ERROR:  One or more StatusEntity records is not for the requested pipeline.", HttpStatus.UNPROCESSABLE_ENTITY);
				}
			} 
			try {
				List<PcpCondensedStatusI> submitList = new ArrayList<>();
				submitList.addAll(entityList);
				_pcpUtils.submitJobs(projectId, pipelineId, submitItem, submitList, submitItem.getParameters(), 
						getSessionUser(), forceProcessLocally);
			} catch (Exception e) {
				_logger.error("Exception", e);
				return new ResponseEntity<>("ERROR:  Process threw an exception - " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			_logger.error("Exception", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	private void updateStatusEntityFromTransferStatus(PcpStatusEntity statusEntity, PcpStatusEntityT statusEntityT) {
		
		final String status = statusEntity.getStatus();
		final String statusT = statusEntityT.getStatus();
		final String statusInfoT = statusEntityT.getStatusInfo();
		
		final Boolean prereqs = statusEntity.getPrereqs();
		final Boolean prereqsT = statusEntityT.getPrereqs();
		final String prereqsInfoT = statusEntityT.getPrereqsInfo();
		
		final Boolean validated = statusEntity.getValidated();
		final Boolean validatedT = statusEntityT.getValidated();
		final String validatedInfoT = statusEntityT.getValidatedInfo();
		
		final Boolean issues = statusEntity.getIssues();
		final Boolean issuesT = statusEntityT.getIssues();
		final String issuesInfoT = statusEntityT.getIssuesInfo();
		
		final Boolean impeded = statusEntity.getImpeded();
		final Boolean impededT = statusEntityT.getImpeded();
		final String impededInfoT = statusEntityT.getImpededInfo();
		
		final String notesT = statusEntityT.getNotes();
		
		
		if (!status.equals(statusT)) {
			statusEntity.setStatus(statusT);
			statusEntity.setIssuesTime(new Date());
		}
		statusEntity.setStatusInfo(statusInfoT);
		
		if (!prereqs.equals(prereqsT)) {
			statusEntity.setPrereqs(prereqsT);
			statusEntity.setPrereqsTime(new Date());
		}
		statusEntity.setPrereqsInfo(prereqsInfoT);
		
		if (!validated.equals(validatedT)) {
			statusEntity.setValidated(validatedT);
			statusEntity.setValidatedTime(new Date());
		}
		statusEntity.setValidatedInfo(validatedInfoT);
		
		if (!issues.equals(issuesT)) {
			statusEntity.setIssues(issuesT);
			statusEntity.setIssuesTime(new Date());
		}
		statusEntity.setIssuesInfo(issuesInfoT);
		
		if (!impeded.equals(impededT)) {
			statusEntity.setImpeded(impededT);
			statusEntity.setImpededTime(new Date());
		}
		statusEntity.setImpededInfo(impededInfoT);
		
		statusEntity.setNotes(notesT);
		
	}
	
	private List<PcpSummaryStatus> generateSummaryListFromStatusEntities(List<PcpStatusEntity> statusEntities, Boolean includeSubgroupSummary) {
		final List<PcpSummaryStatus> returnList = new ArrayList<>();
		final Map<PcpProjectPipelineEntity,PcpStatusEntityT> entityMap = new HashMap<>();
		for (final PcpStatusEntity entity : statusEntities) {
			final String entityStatus = (entity.getStatus() != null) ? entity.getStatus() : "";
			final PcpStatusEntityT entitySummary = getEntitySummaryForEntity(entityMap,entity);
			if (!entity.getPrereqs()) {
				entitySummary.setPrereqs(false);
				entitySummary.setStatus("NOT_SUBMITTED");
				entitySummary.setValidated(false);
				entitySummary.setImpeded(false);
				entitySummary.setIssues(false);
				entitySummary.setValidated(false);
			} else if ((entityStatus.equals("RUNNING") || entityStatus.equals("QUEUED")) && 
					entitySummary.getPrereqs() && !entitySummary.equals("RUNNING")) {
				entitySummary.setStatus("RUNNING");
				entitySummary.setValidated(false);
				entitySummary.setImpeded(false);
				entitySummary.setIssues(false);
				entitySummary.setValidated(false);
			} else if (entity.getImpeded() && entitySummary.getPrereqs() && 
					!entitySummary.equals("RUNNING") && !entitySummary.getImpeded()) {
				entitySummary.setImpeded(true);
				entitySummary.setIssues(false);
				entitySummary.setValidated(false);
			} else if (entity.getIssues() && entitySummary.getPrereqs() && 
					!entitySummary.equals("RUNNING") && !entitySummary.getImpeded() && !entitySummary.getIssues()) {
				entitySummary.setIssues(true);
				entitySummary.setValidated(false);
			} else if (!entity.getValidated()) {
				entitySummary.setValidated(false);
			}
		}
		final List<PcpStatusEntityT> statusEntitiesT = new ArrayList<>();
		statusEntitiesT.addAll(entityMap.values());
		final List<PcpSummaryStatus> summaryList = new ArrayList<>();
		for (final PcpStatusEntityT entityT : statusEntitiesT) {
			final PcpSummaryStatus summary = getEntryForEntity(summaryList,entityT);
			incrementSummaryValues(summary,entityT);
		}
		returnList.addAll(summaryList);
		if (includeSubgroupSummary) {
			final List<PcpSummaryStatus> subGroupList = new ArrayList<>();
			for (final PcpStatusEntity entity : statusEntities) {
				final PcpSummaryStatus summary = getEntryForEntity(subGroupList,entity);
				incrementSummaryValues(summary,entity);
			}
			returnList.addAll(subGroupList);
		}
		return returnList;
	}

	private void incrementSummaryValues(PcpSummaryStatus summary, PcpStatusEntityI entity) {
		
		final String entityStatus = entity.getStatus();
		if (!entity.getStatus().equals("REMOVED")) {
			summary.incrementTotal();
		}
		if (!entity.getPrereqs()) {
			summary.incrementNotReady();
		} else if (entityStatus.equals("RUNNING") || entityStatus.equals("QUEUED")) {
			summary.incrementRunning();
		} else if (entity.getImpeded()) {
			summary.incrementImpeded();
		} else if (entity.getIssues()) {
			summary.incrementIssues();
		} else if (entity.getValidated()) {
			summary.incrementComplete();
		} else if (entity.getPrereqs()) {
			summary.incrementReady();
		}
	}

	private PcpStatusEntityT getEntitySummaryForEntity(Map<PcpProjectPipelineEntity, PcpStatusEntityT> entityMap, PcpStatusEntity entity) {
		final PcpProjectPipelineEntity ppEntity = entity.getProjectPipelineEntity();
		if (entityMap.containsKey(ppEntity)) {
			return entityMap.get(ppEntity);
		}
		final PcpStatusEntityT newEntity = new PcpStatusEntityT();
		newEntity.setProject(entity.getProject());
		newEntity.setPipeline(entity.getPipeline());
		newEntity.setSubGroup(SUMMARY_STATUS);
		newEntity.setPrereqs(true);
		newEntity.setStatus("NOT_SUBMITTED");
		newEntity.setImpeded(false);
		newEntity.setIssues(false);
		newEntity.setValidated(true);
		entityMap.put(ppEntity,newEntity);
		return newEntity;
	}

	private PcpSummaryStatus getEntryForEntity(List<PcpSummaryStatus> returnList, PcpStatusEntityI entity) {
		for (final PcpSummaryStatus status : returnList) {
			if (status.getPipeline().equals(entity.getPipeline()) && status.getSubGroup().equals(entity.getSubGroup())) {
				return status;
			}
		}
		final PcpSummaryStatus newStatus = new PcpSummaryStatus(entity.getPipeline(),entity.getSubGroup());
		returnList.add(newStatus);
		return newStatus;
	}

	private boolean isOnDemandStatusPipeline(String projectId, String pipelineId) {
		if (!disabledCache.containsKey(projectId)) {
			disabledCache.put(projectId, new HashMap<String,String>());
		}
		if (disabledCache.get(projectId).containsKey(pipelineId)) {
			return (disabledCache.get(projectId).get(pipelineId).equals(PcpConfigConstants.FREQUENCY_DISABLE_CACHING));
		}
		final Configuration conf = _configService.getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, Scope.Project, projectId);
		if (conf==null) {
			_logger.error("ERROR:  Could not obtain configuration for " + PcpConfigConstants.CONFIG_TOOL + 
				" (PROJECT=" + projectId + ", PIPELINE=" + pipelineId + ").");
			return true;
		}
    	final List<PcpConfigInfo> configList = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
    	for (final PcpConfigInfo config : configList) {
    		if (config != null && config.getPipeline() != null && config.getPipeline().equals(pipelineId)) {
    			disabledCache.get(projectId).put(pipelineId, config.getUpdateFrequency());
    			return (config.getUpdateFrequency()!=null && config.getUpdateFrequency().equals(PcpConfigConstants.FREQUENCY_DISABLE_CACHING));
    		}
    	}
		_logger.warn("WARNING:  Could not determine update frequency for " + PcpConfigConstants.CONFIG_TOOL + 
				" (PROJECT=" + projectId + ", PIPELINE=" + pipelineId + ").");
		return true;
	}
	
	public static void clearOnDemandCache(final String projectId) {
		if (disabledCache.containsKey(projectId) && disabledCache.get(projectId)!=null) {
			disabledCache.get(projectId).clear();
		}
	}
	
}
