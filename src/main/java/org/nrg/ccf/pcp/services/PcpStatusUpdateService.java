package org.nrg.ccf.pcp.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.dto.ComponentSet;
import org.nrg.ccf.pcp.dto.ExecutionGroup;
import org.nrg.ccf.pcp.dto.PcpProjectPipelineEntity;
import org.nrg.ccf.pcp.dto.PcpProjectPipelineSubgroupEntity;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.exception.PcpEntityNotFoundException;
import org.nrg.ccf.pcp.exception.PcpUpdateRunningException;
import org.nrg.ccf.pcp.inter.*;
import org.nrg.ccf.pcp.utils.PcpUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class PcpStatusUpdateService {
	
	private final PcpStatusEntityService _statusEntityService;
	private final PcpUtils _pcpUtils;
	private static ConcurrentHashMap<String,CopyOnWriteArrayList<String>> _cacheMap = new ConcurrentHashMap<>();
	private static final Logger _logger = LoggerFactory.getLogger(PcpStatusUpdateService.class);

	@Autowired
	public PcpStatusUpdateService(final PcpStatusEntityService statusEntityService, final PcpUtils pcpUtils) {
		super();
		_statusEntityService = statusEntityService;
		_pcpUtils = pcpUtils;
	}
	
	public class UpdateRunnable implements Runnable {

		private List<PcpStatusEntity> statusEntities;
		private UserI user;
		private ComponentSet componentSet;
		private Integer completed = 0;
		private boolean sendEmail;
		private String projectId;
		private String pipelineId;
		
		public UpdateRunnable(String projectId, String pipelineId, List<PcpStatusEntity> statusEntities,
				UserI user, ComponentSet componentSet) {
			super();
			this.projectId = projectId;
			this.pipelineId = pipelineId;
			this.statusEntities = statusEntities;
			this.user = user;
			this.componentSet = componentSet;
		}

		@Override
		public void run() {
			try {
				completed = 0;
				_logger.debug("Start time - " + new Date());
				for (final PcpStatusEntity statusEntity : statusEntities) {
					try {
						refreshStatusEntityCheckAndValidate(statusEntity, user, componentSet);
					} catch (PcpComponentSetException e) {
						_logger.error("ComponentSet exception - updateEntityStatus - " + statusEntity);
					} catch (Throwable t) {
						_logger.error("PcpStatusUpdateService - ProcessingException - " +  statusEntity + " - " + t);
						_logger.error(ExceptionUtils.getFullStackTrace(t));
					}
					completed++;
				}
				if (sendEmail) {
					StringBuilder sb = new StringBuilder();
					sb.append("<h3>PCP Status Update Refresh Completed (PROJECT=" + projectId +
							", PIPELINE=" + pipelineId +")</h3><br>");
					sb.append(TurbineUtils.GetFullServerPath());
					sb.append("/app/template/Page.vm?view=pcp&project=" + projectId +
							"&pipeline=" + pipelineId);
					_logger.debug("Sending e-mail - " + new Date());
					AdminUtils.sendUserHTMLEmail("PCP Status Update Refresh Completed", sb.toString(), false, new String[] { user.getEmail() });
				}
				_logger.debug("Thread run complete - " + getPctComplete());
			} finally {
				setNotRunning(projectId, pipelineId);
			}
		}
		
		private float getPctComplete() {
			return completed.floatValue()/(float)(statusEntities.size());
		}

		public void setSendEmail(boolean sendEmail) {
			this.sendEmail = sendEmail;
		}
		
	}
	
	public int updateProjectStatus(final String projectId, final String pipelineId, final UserI user,  
			final Boolean dontWait, final Boolean emailForEarlyReturn) 
				throws PcpComponentSetException, PcpUpdateRunningException {
		return updateProjectStatus(projectId, pipelineId, user, null, dontWait, emailForEarlyReturn);
	}

	public int updateProjectStatus(final String projectId, final String pipelineId, final UserI user, List<PcpCondensedStatusI> entityList, 
			final Boolean dontWait, final Boolean emailForEarlyReturn) 
				throws PcpComponentSetException, PcpUpdateRunningException {
		if (isRunningIfNotSetToRunning(projectId,pipelineId)) {
			throw new PcpUpdateRunningException("Project status update already running for this pipeline");
		}
		final ComponentSet componentSet = _pcpUtils.getComponentSet(projectId, pipelineId);
		final PipelineSelectorI selector = componentSet.getSelector();
		final List<PcpStatusEntity> statusEntities = selector.getStatusEntities(_statusEntityService, projectId, pipelineId, user);
		if (entityList != null) {
			final List<PcpProjectPipelineSubgroupEntity> ppseList = new ArrayList();
			for (final PcpCondensedStatusI status : entityList) {
				ppseList.add(new PcpProjectPipelineSubgroupEntity(status.getProject(), status.getPipeline(),
						status.getSubGroup(), status.getEntityId()));
			}
			final Iterator<PcpStatusEntity> i = statusEntities.iterator();
			while (i.hasNext()) {
				final PcpStatusEntity entity = i.next();
				if (!ppseList.contains(entity.getProjectPipelineSubgroupEntity())) {
					i.remove();
				}
			}
		}
		Collections.sort(statusEntities);
		UpdateRunnable runnable = new UpdateRunnable(projectId, pipelineId, statusEntities, user, componentSet);
		Thread thread = new Thread(runnable);
		thread.start();
		if (!dontWait) {
			try {
				thread.join(0);
			} catch (InterruptedException e) {
				setNotRunning(projectId, pipelineId);
				_logger.warn("UpdateRunnable thread interrupted");
			}
		} else {
			try {
				_logger.debug("Thread join - " + new Date());
				thread.join(5000);
				_logger.debug("Thread join ended - " + new Date());
				if (thread.isAlive()) {
					final float pctComplete = runnable.getPctComplete();
					if (runnable.getPctComplete()<.10) {
						runnable.setSendEmail(emailForEarlyReturn);
						// Seems to run faster towards the end.
						final int estComplete = (int)((5/pctComplete)-10);
						_logger.debug("Completion percent - " + pctComplete + ", Estimated completion seconds - " + estComplete);
						return estComplete;
					} else {
						_logger.debug("Thread rejoined - " + new Date());
						thread.join(0);
					}
				}
			} catch (InterruptedException e) {
				setNotRunning(projectId, pipelineId);
				_logger.warn("UpdateRunnable thread interrupted");
			}
		}
		return -1;
		
	}

	public void updateProjectStatus(String projectId, String pipelineId, UserI user) 
				throws PcpComponentSetException, PcpUpdateRunningException {
		updateProjectStatus(projectId, pipelineId, user, false, false);
	}
	
	private synchronized boolean isRunningIfNotSetToRunning(String projectId, String pipelineId) {
		if (!_cacheMap.containsKey(projectId)) {
			_cacheMap.put(projectId, new CopyOnWriteArrayList<String>());
		}
		if (_cacheMap.get(projectId).contains(pipelineId)) {
			return true;
		} else {
			_cacheMap.get(projectId).add(pipelineId);
			return false;
		}
	}
	
	private synchronized void setNotRunning(String projectId, String pipelineId) {
		if (_cacheMap.containsKey(projectId)) {
			_cacheMap.get(projectId).remove(pipelineId);
		}
	}

	public void refreshStatusEntityCheckAndValidate(PcpStatusEntity statusEntity, UserI user) throws  PcpComponentSetException {
			final ComponentSet componentSet = _pcpUtils.getComponentSet(statusEntity.getProject(), statusEntity.getPipeline());
			refreshStatusEntityCheckAndValidate(statusEntity, user, componentSet);
	}

	private void refreshStatusEntityCheckAndValidate(PcpStatusEntity statusEntity, UserI user, ComponentSet componentSet) throws  PcpComponentSetException {
			final PipelinePrereqCheckerI prereqChecker = componentSet.getPrereqChecker();
			final PipelineStatusUpdaterI statusUpdater = componentSet.getStatusUpdater();
			final PipelineValidatorI validator = componentSet.getValidator();
			_statusEntityService.refresh(statusEntity);
			final String currentStatus = statusEntity.getStatus();
			// If entity is currently submitted or running, we'll let that process do the validation.
			if (currentStatus==null || !(currentStatus.equals(PcpConstants.PcpStatus.RUNNING.toString()) || 
					currentStatus.equals(PcpConstants.PcpStatus.SUBMITTED.toString()))) {
				prereqChecker.checkPrereqs(statusEntity, user);
				validator.validate(statusEntity, user);
				// Update status last.  Its calculation is based on current results.
				statusUpdater.updateStatus(statusEntity, user);
				_statusEntityService.update(statusEntity);
			}
	}

	public void runValidation(String projectId, String pipelineId, ExecutionGroup executionGroup, UserI sessionUser,
			ComponentSet componentSet) throws PcpEntityNotFoundException {
		final PipelineValidatorI validator = componentSet.getValidator();
		final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(projectId, pipelineId, executionGroup);
		if (statusEntity==null) {
			throw new PcpEntityNotFoundException();
		}
		validator.validate(statusEntity,  sessionUser);
		_statusEntityService.update(statusEntity);
	}

	public void setStatus(String projectId, String pipelineId, ExecutionGroup executionGroup,
			UserI sessionUser, PcpConstants.PcpStatus status) throws PcpEntityNotFoundException {
		final PcpStatusEntity statusEntity = _statusEntityService.getStatusEntity(projectId, pipelineId, executionGroup);
		if (statusEntity==null) {
			throw new PcpEntityNotFoundException();
		}
		statusEntity.setStatus(status.toString());
		statusEntity.setStatusTime(new Date());
		_statusEntityService.update(statusEntity);
	}
	
}
