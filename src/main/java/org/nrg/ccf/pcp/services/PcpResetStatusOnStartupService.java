package org.nrg.ccf.pcp.services;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.nrg.ccf.pcp.anno.PipelineExecManager;
import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.constants.PcpConstants.PcpStatus;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PcpResetStatusOnStartupService {
	
	private final PcpStatusEntityService _statusEntityService;
	private Map<String,Boolean> _resetMap = new HashMap<>();

	@Autowired
	public PcpResetStatusOnStartupService(final PcpStatusEntityService statusEntityService) {
		super();
		_statusEntityService = statusEntityService;
	}
	
	@PostConstruct
	public void resetStatusOnStartup() {
		final List<PcpStatusEntity> entities = _statusEntityService.getAllSubmittedOrRunning();
		final RuntimeMXBean mxBean = ManagementFactory.getRuntimeMXBean();
		if (mxBean.getUptime()<3000000) {
			for (PcpStatusEntity entity : entities) {
				if (isConfiguredForReset(entity)) {
					final String formerStatus = entity.getStatus();
					entity.setStatus(PcpConstants.PcpStatus.RESET.toString());
					entity.setStatusInfo("Status reset due to web server restart (FORMER_STATUS=" + formerStatus + ").");
					entity.setStatusTime(new Date());
					_statusEntityService.update(entity);
				}
			}
		}
	}

	private boolean isConfiguredForReset(PcpStatusEntity entity) {
		final String execManager = entity.getSubmitExecManager();
		if (!_resetMap.containsKey(execManager)) {
			try {
				final Class<?> emClass = Class.forName(execManager);
		        if (emClass.isAnnotationPresent(PipelineExecManager.class)) {
		            final PipelineExecManager emAnno = emClass.getAnnotation(PipelineExecManager.class);
		            _resetMap.put(execManager, emAnno.resetStatusOnStartup());
		        } else {
		            _resetMap.put(execManager, false);
		        }
			} catch (ClassNotFoundException e) {
				_resetMap.put(execManager, false);
			}
		}
		return _resetMap.get(execManager);
		
	}
}
