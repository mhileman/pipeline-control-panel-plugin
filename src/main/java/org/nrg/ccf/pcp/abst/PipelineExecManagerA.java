package org.nrg.ccf.pcp.abst;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineExecManagerI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.queue.PcpQueueMonitor;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.framework.node.XnatNode;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class PipelineExecManagerA implements PipelineExecManagerI {
	
	private PcpStatusEntityService _statusEntityService = XDAT.getContextService().getBean(PcpStatusEntityService.class);
	private XnatNode _xnatNode = XDAT.getContextService().getBean(XnatNode.class);
	private static final Logger _logger = LoggerFactory.getLogger(PipelineExecManagerA.class);

	@Override
	public void submitJobs(PipelineSubmitterI submitter, PipelineValidatorI validator,
			List<PcpCondensedStatusI> statusList, Map<String, String> parameters, UserI user) {
		final Date submitTime = new Date();
		final Iterator<PcpCondensedStatusI> i = statusList.iterator();
		while (i.hasNext()) {
			final PcpCondensedStatusI status = i.next();
			final PcpStatusEntity entity = _statusEntityService.getStatusEntity(status);
			if (entity.getStatus().equals(PcpConstants.PcpStatus.SUBMITTED.toString())) {
				_logger.info("Entity " + entity + " has already been submitted.  Skipping.");
				i.remove();
				continue;
			}
			entity.setStatus(PcpConstants.PcpStatus.SUBMITTED.toString());
			entity.setSubmitNode(_xnatNode.getNodeId());
			entity.setSubmitExecManager(this.getClass().getCanonicalName());
			entity.setStatusTime(submitTime);
			entity.setStatusInfo("Submitted by the " + this.getClass().getSimpleName() + " exec manager.");
			_statusEntityService.update(entity);
		}
		if (statusList==null || statusList.size()<0) {
			return;
		}
		final PcpQueueMonitor queueMonitor = new PcpQueueMonitor(statusList);
		try {
			while (!queueMonitor.mayProceed()) {
				_logger.debug("WAIT:  " + this.toString() + ", PROJECT=" + statusList.get(0).getProject() +
						", PIPELINE=" + statusList.get(0).getPipeline() + ", QUEUEMONITOR=" + queueMonitor.getId());
				if (queueMonitor.isCancelled()) {
					markCancelled(statusList);
					return;
				}
				try {
					TimeUnit.MINUTES.sleep(1);
				} catch (InterruptedException e) {
					markCancelled(statusList);
					_logger.error("ERROR:  Thread wait interrupted.  " + this.toString() + ", PROJECT=" + statusList.get(0).getProject() +
						", PIPELINE=" + statusList.get(0).getPipeline());
					return;
				}
			}
			_logger.debug("CONTINUE:  " + this.toString() + ", PROJECT=" + statusList.get(0).getProject() +
					", PIPELINE=" + statusList.get(0).getPipeline() + ", QUEUEMONITOR=" + queueMonitor.getId());
			doSubmit(submitter, validator, statusList, parameters, user);
		} finally {
			_logger.debug("FINISHED:  " + this.toString() + ", PROJECT=" + statusList.get(0).getProject() +
					", PIPELINE=" + statusList.get(0).getPipeline() + ", QUEUEMONITOR=" + queueMonitor.getId());
			queueMonitor.notifyFinished();
		}
	}
	
	private void markCancelled(List<PcpCondensedStatusI> statusList) {
		final Date cancelTime = new Date();
		for (final PcpCondensedStatusI status : statusList) {
			final PcpStatusEntity entity = _statusEntityService.getStatusEntity(status);
			entity.setStatus(PcpConstants.PcpStatus.CANCELLED.toString());
			entity.setSubmitNode(_xnatNode.getNodeId());
			entity.setSubmitExecManager(this.getClass().getCanonicalName());
			entity.setStatusTime(cancelTime);
			entity.setStatusInfo("Submit cancelled by the " + this.getClass().getSimpleName() + " exec manager.");
			_statusEntityService.update(entity);
		}
	}

	public abstract void doSubmit(PipelineSubmitterI submitter, PipelineValidatorI validator,
			List<PcpCondensedStatusI> statusList, Map<String, String> parameters, UserI user);
	
	public PcpStatusEntityService getStatusEntityService() {
		return _statusEntityService;
	}

}
