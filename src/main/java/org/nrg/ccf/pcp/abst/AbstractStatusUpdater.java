package org.nrg.ccf.pcp.abst;

import java.util.Date;

import org.nrg.ccf.pcp.constants.PcpConstants;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PipelineStatusUpdaterI;
import org.nrg.xft.security.UserI;

public abstract class AbstractStatusUpdater implements PipelineStatusUpdaterI {
	
	@Override
	public void updateStatus(PcpStatusEntity statusEntity, UserI user) {
		if (statusEntity.getSubmitTime() == null) {
			if (statusEntity.getValidated() != null && statusEntity.getValidated()) {
				statusEntity.setStatus(PcpConstants.PcpStatus.EXT_COMPLETE.toString());
			} else {
				statusEntity.setStatus(PcpConstants.PcpStatus.NOT_SUBMITTED.toString());
			}
			statusEntity.setStatusTime(new Date());
		}
		if (statusEntity.getStatus() != null && statusEntity.getStatus().equals(PcpConstants.PcpStatus.NOT_SUBMITTED.toString()) &&
				statusEntity.getValidated() != null && statusEntity.getValidated()) {
			statusEntity.setStatus(PcpConstants.PcpStatus.EXT_COMPLETE.toString());
			statusEntity.setStatusTime(new Date());
		}
	}
	
	public boolean isSubmittedOrRunningStatus(PcpConstants.PcpStatus status) {
		return (status.equals(PcpConstants.PcpStatus.SUBMITTED) || status.equals(PcpConstants.PcpStatus.QUEUED) ||
				status.equals(PcpConstants.PcpStatus.RUNNING));
		
	}
	
	public boolean isSubmittedOrRunningStatus(String status) {
		return (status.equals(PcpConstants.PcpStatus.SUBMITTED.toString()) || status.equals(PcpConstants.PcpStatus.QUEUED.toString()) ||
				status.equals(PcpConstants.PcpStatus.RUNNING.toString()));
	}
	
}
