package org.nrg.ccf.pcp.scheduler;

import org.nrg.ccf.pcp.preferences.PcpPreferences;
import org.nrg.ccf.pcp.scheduler.tasks.PcpStatusUpdate;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.config.TriggerTask;
import org.springframework.scheduling.support.CronTrigger;

@Configuration
@EnableScheduling
public class PcpScheduler {
	
    @Bean
    public TriggerTask statusUpdateTrigger(final PcpStatusUpdateService statusUpdateService, final PcpPreferences pcpPreferences,
    		final ConfigService configService, final XnatTaskService taskService, UserManagementServiceI userManagementService) {
        return new TriggerTask(new PcpStatusUpdate(statusUpdateService, pcpPreferences, configService,
        		taskService, userManagementService), new CronTrigger("0 */15 * * * *"));
    }

}
