package org.nrg.ccf.pcp.dto;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;

public class PcpCondensedStatus implements PcpCondensedStatusI {
	
	private String project;
	private String pipeline;
	private String entityType;
	private String entityId;
	private String entityLabel;
	private String subGroup;
	private String status;
	private Boolean prereqs;
	private Boolean validated;
	private Boolean issues;
	private Boolean impeded;
	
	
	public PcpCondensedStatus() {
		super();
	}
	
	public PcpCondensedStatus(PcpStatusEntity statusEntity) {
		super();
		this.project = statusEntity.getProject();
		this.pipeline = statusEntity.getPipeline();
		this.entityType = statusEntity.getEntityType();
		this.entityId = statusEntity.getEntityId();
		this.entityLabel = statusEntity.getEntityLabel();
		this.subGroup = statusEntity.getSubGroup();
		this.status = statusEntity.getStatus();
		this.prereqs = statusEntity.getPrereqs();
		this.validated = statusEntity.getValidated();
		this.issues = statusEntity.getIssues();
		this.impeded = statusEntity.getImpeded();
	}

	@Override
	public String getProject() {
		return this.project;
	}

	@Override
	public void setProject(String project) {
		this.project = project;
		
	}

	@Override
	public String getPipeline() {
		return this.pipeline;
	}

	@Override
	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}

	@Override
	public String getEntityType() {
		return entityType;
	}
	
	@Override
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
	@Override
	public String getEntityId() {
		return entityId;
	}
	
	@Override
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	@Override
	public String getEntityLabel() {
		return entityLabel;
	}
	
	@Override
	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}
	
	@Override
	public String getSubGroup() {
		return subGroup;
	}
	
	@Override
	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}
	
	@Override
	public String getStatus() {
		return status;
	}
	
	@Override
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public Boolean getPrereqs() {
		return prereqs;
	}
	
	@Override
	public void setPrereqs(Boolean prereqs) {
		this.prereqs = prereqs;
	}
	
	@Override
	public Boolean getValidated() {
		return validated;
	}
	
	@Override
	public void setValidated(Boolean validated) {
		this.validated = validated;
	}
	
	@Override
	public Boolean getIssues() {
		return issues;
	}
	
	@Override
	public void setIssues(Boolean issues) {
		this.issues = issues;
	}
	
	@Override
	public Boolean getImpeded() {
		return impeded;
	}
	
	@Override
	public void setImpeded(Boolean impeded) {
		this.impeded = impeded;
	}
	
	public String toString() {
		return "PcpCondensedStatus - (PROJECT=" + project + ", PIPELINE=" + pipeline + ", ENTITY=" + entityLabel + ", SUBGROUP=" + subGroup + ")";
	}

	@Override
	public int compareTo(PcpCondensedStatusI status) {
		int cmp = project.compareTo(status.getProject());
		if (cmp != 0) return cmp;
		cmp = pipeline.compareTo(status.getPipeline());
		if (cmp != 0) return cmp;
		cmp = entityLabel.compareTo(status.getEntityLabel());
		if (cmp != 0) return cmp;
		cmp = subGroup.compareTo(status.getSubGroup());
		return cmp;
	}

}
