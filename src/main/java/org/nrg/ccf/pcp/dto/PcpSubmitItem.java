package org.nrg.ccf.pcp.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.inter.PcpStatusEntityI;

public class PcpSubmitItem {
	
	final Map<String,String> parameters = new HashMap<>();
	final List<PcpCondensedStatus> entities = new ArrayList<>();
	
	public Map<String,String> getParameters() {
		return parameters;
	}
	
	public List<PcpCondensedStatus> getEntities() {
		return entities;
	}
	
}
