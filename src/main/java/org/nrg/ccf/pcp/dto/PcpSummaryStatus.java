package org.nrg.ccf.pcp.dto;

public class PcpSummaryStatus implements Comparable<PcpSummaryStatus> {
	
	public String pipeline = "";
	public String subGroup = "";
	public Integer notReady = 0;
	public Integer ready = 0;
	public Integer running = 0;
	public Integer issues = 0;
	public Integer impeded = 0;
	public Integer complete = 0;
	public Integer total = 0;
	
	public PcpSummaryStatus(String pipeline, String subGroup) {
		this.pipeline = pipeline;
		this.subGroup = subGroup;
	}

	public String getPipeline() {
		return pipeline;
	}
	
	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}
	
	public String getSubGroup() {
		return subGroup;
	}
	
	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}
	
	public Integer getNotReady() {
		return notReady;
	}
	
	public void setNotReady(Integer notReady) {
		this.notReady = notReady;
	}
	
	public void incrementNotReady() {
		this.notReady += 1;
	}
	
	public Integer getReady() {
		return ready;
	}
	
	public void setReady(Integer ready) {
		this.ready = ready;
	}
	
	public void incrementReady() {
		this.ready += 1;
	}
	
	public Integer getRunning() {
		return running;
	}
	
	public void setRunning(Integer running) {
		this.running = running;
	}
	
	public void incrementRunning() {
		this.running += 1;
	}
	
	public Integer getIssues() {
		return issues;
	}
	
	public void setIssues(Integer issues) {
		this.issues = issues;
	}
	
	public void incrementIssues() {
		this.issues += 1;
	}
	
	public Integer getImpeded() {
		return impeded;
	}
	
	public void setImpeded(Integer impeded) {
		this.impeded = impeded;
	}
	
	public void incrementImpeded() {
		this.impeded += 1;
	}
	
	public Integer getComplete() {
		return complete;
	}
	
	public void setComplete(Integer complete) {
		this.complete = complete;
	}
	
	public void incrementComplete() {
		this.complete += 1;
	}
	
	public Integer getTotal() {
		return total;
	}
	
	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public void incrementTotal() {
		this.total += 1;
	}

	@Override
	public int compareTo(PcpSummaryStatus summary) {
		int cmp = pipeline.compareTo(summary.getPipeline());
		if (cmp != 0) return cmp;
		cmp = subGroup.compareTo(summary.getSubGroup());
		return cmp;
	}

}
