package org.nrg.ccf.pcp.dto;

import java.util.Date;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.inter.PcpStatusEntityI;

public class PcpStatusEntityT implements PcpStatusEntityI {

	private String project = "";
	private String pipeline = "";
	private String entityType = "";
	private String entityId = "";
	private String entityLabel = "";
	private String subGroup = "";
	private Date submitTime = new Date(0);
	private String submitNode = "";
	private String submitExecManager = "";
	
	private String status = "";
	private Date statusTime = new Date(0);
	private String statusInfo = "";
	
	private Boolean prereqs = false;
	private Date prereqsTime = new Date(0);
	private String prereqsInfo = "";
	
	private Boolean validated = false;
	private Date validatedTime;
	private String validatedInfo;
	
	private Boolean issues = false;
	private Date issuesTime = new Date(0);
	private String issuesInfo = "";
	
	private Boolean impeded = false;
	private Date impededTime = new Date(0);
	private String impededInfo = "";
	
	private String notes = "";
	
	public PcpStatusEntityT() {
		// Used by XAPI
	}
	
	public PcpStatusEntityT(final PcpStatusEntity statusEntity) {
		this.project=statusEntity.getProject();
		this.pipeline=statusEntity.getPipeline();
		this.entityType=statusEntity.getEntityType();
		this.entityId=statusEntity.getEntityId();
		this.entityLabel=statusEntity.getEntityLabel();
		this.subGroup=statusEntity.getSubGroup();
		this.submitTime=statusEntity.getSubmitTime();
		this.submitNode=statusEntity.getSubmitNode();
		this.submitExecManager=statusEntity.getSubmitExecManager();
		this.status=statusEntity.getStatus();
		this.statusTime=statusEntity.getStatusTime();
		this.statusInfo=statusEntity.getStatusInfo();
		this.prereqs=statusEntity.getPrereqs();
		this.prereqsTime=statusEntity.getPrereqsTime();
		this.prereqsInfo=statusEntity.getPrereqsInfo();
		this.validated=statusEntity.getValidated();
		this.validatedTime=statusEntity.getValidatedTime();
		this.validatedInfo=statusEntity.getValidatedInfo();
		this.issues=statusEntity.getIssues();
		this.issuesTime=statusEntity.getIssuesTime();
		this.issuesInfo=statusEntity.getIssuesInfo();
		this.impeded=statusEntity.getImpeded();
		this.impededTime=statusEntity.getImpededTime();
		this.impededInfo=statusEntity.getImpededInfo();
		this.notes=statusEntity.getNotes();
	}
	
	@Override
	public String getProject() {
		return project;
	}

	@Override
	public void setProject(String project) {
		this.project = project;
	}

	@Override
	public String getPipeline() {
		return pipeline;
	}

	@Override
	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}
	
	@Override
	public String getEntityType() {
		return entityType;
	}

	@Override
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
	@Override
	public String getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	@Override
	public String getEntityLabel() {
		return entityLabel;
	}

	@Override
	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}

	@Override
	public String getSubGroup() {
		return subGroup;
	}

	@Override
	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}

	@Override
	public Date getSubmitTime() {
		return submitTime;
	}

	@Override
	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	@Override
	public String getSubmitNode() {
		return submitNode;
	}

	@Override
	public void setSubmitNode(String submitNode) {
		this.submitTime = submitTime;
	}

	@Override
	public String getSubmitExecManager() {
		return submitExecManager;
	}

	@Override
	public void setSubmitExecManager(String submitExecManager) {
		this.submitExecManager = submitExecManager;
	}

	@Override
	public String getStatus() {
		return status;
	}

	@Override
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public Date getStatusTime() {
		return statusTime;
	}

	@Override
	public void setStatusTime(Date statusTime) {
		this.statusTime = statusTime;
	}

	public String getStatusInfo() {
		return statusInfo;
	}

	@Override
	public void setStatusInfo(String statusInfo) {
		this.statusInfo = statusInfo;
	}

	@Override
	public Boolean getPrereqs() {
		return prereqs;
	}

	@Override
	public void setPrereqs(Boolean prereqs) {
		this.prereqs = prereqs;
	}

	@Override
	public Date getPrereqsTime() {
		return prereqsTime;
	}

	@Override
	public void setPrereqsTime(Date prereqsTime) {
		this.prereqsTime = prereqsTime;
	}

	@Override
	public String getPrereqsInfo() {
		return prereqsInfo;
	}

	@Override
	public void setPrereqsInfo(String prereqsInfo) {
		this.prereqsInfo = prereqsInfo;
	}

	@Override
	public Boolean getValidated() {
		return validated;
	}

	@Override
	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	@Override
	public Date getValidatedTime() {
		return validatedTime;
	}

	@Override
	public void setValidatedTime(Date validatedTime) {
		this.validatedTime = validatedTime;
	}

	@Override
	public String getValidatedInfo() {
		return validatedInfo;
	}

	@Override
	public void setValidatedInfo(String validatedInfo) {
		this.validatedInfo = validatedInfo;
	}

	@Override
	public Boolean getIssues() {
		return issues;
	}

	@Override
	public void setIssues(Boolean issues) {
		this.issues = issues;
	}

	@Override
	public Date getIssuesTime() {
		return issuesTime;
	}

	@Override
	public void setIssuesTime(Date issuesTime) {
		this.issuesTime = issuesTime;
	}

	@Override
	public String getIssuesInfo() {
		return issuesInfo;
	}

	@Override
	public void setIssuesInfo(String issuesInfo) {
		this.issuesInfo = issuesInfo;
	}

	@Override
	public Boolean getImpeded() {
		return impeded;
	}

	@Override
	public void setImpeded(Boolean impeded) {
		this.impeded = impeded;
	}

	@Override
	public Date getImpededTime() {
		return impededTime;
	}

	@Override
	public void setImpededTime(Date impededTime) {
		this.impededTime = impededTime;
	}

	@Override
	public String getImpededInfo() {
		return impededInfo;
	}

	@Override
	public void setImpededInfo(String impededInfo) {
		this.impededInfo = impededInfo;
	}

	@Override
	public String getNotes() {
		return notes;
	}

	@Override
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Override
	public String toString() {
		return "PcpStatusEntityT - (PROJECT=" + project + ", PIPELINE=" + pipeline + ", ENTITY=" + entityLabel + ", SUBGROUP=" + subGroup + ")";
	}

}
