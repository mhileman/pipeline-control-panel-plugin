package org.nrg.ccf.pcp.dto;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;

public class PcpSubmitStatus {

	private Integer totalRunningJobs;
	private Map<String, Map<Integer, List<PcpCondensedStatusI>>> runningJobs;
	private Map<String, Map<Integer, List<PcpCondensedStatusI>>> waitingJobs;

	public Integer getTotalRunningJobs() {
		return totalRunningJobs;
	}

	public void setTotalRunningJobs(Integer totalRunningJobs) {
		this.totalRunningJobs = totalRunningJobs;
	}

	public Map<String, Map<Integer, List<PcpCondensedStatusI>>> getRunningJobs() {
		return runningJobs;
	}
	
	public void setRunningJobs(Map<String, Map<Integer, List<PcpCondensedStatusI>>> runningJobs) {
		this.runningJobs = runningJobs;
		
	}

	public Map<String, Map<Integer, List<PcpCondensedStatusI>>> getWaitingJobs() {
		return waitingJobs;
	}

	public void setWaitingJobs(Map<String, Map<Integer, List<PcpCondensedStatusI>>> waitingJobs) {
		this.waitingJobs = waitingJobs;
		
	}

}
