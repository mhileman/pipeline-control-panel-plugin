package org.nrg.ccf.pcp.dto;

public class PcpSettingsInfo {
	
	private Boolean pcpEnabled;

	public Boolean getPcpEnabled() {
		return pcpEnabled;
	}

	public void setPcpEnabled(Boolean pcpEnabled) {
		this.pcpEnabled = pcpEnabled;
	}

}
