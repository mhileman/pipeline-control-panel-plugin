package org.nrg.ccf.pcp.exception;

public class PcpSubmitException extends Exception {
	
	private static final long serialVersionUID = -5434943926885692411L;

	public PcpSubmitException() {
		super();
	}

	public PcpSubmitException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PcpSubmitException(String message, Throwable cause) {
		super(message, cause);
	}

	public PcpSubmitException(String message) {
		super(message);
	}

	public PcpSubmitException(Throwable cause) {
		super(cause);
	}

}
