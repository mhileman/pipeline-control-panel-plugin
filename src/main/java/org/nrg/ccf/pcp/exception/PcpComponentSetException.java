package org.nrg.ccf.pcp.exception;

public class PcpComponentSetException extends PcpException {

	private static final long serialVersionUID = 6730341569388865562L;

	public PcpComponentSetException() {
		super();
	}

	public PcpComponentSetException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PcpComponentSetException(String message, Throwable cause) {
		super(message, cause);
	}

	public PcpComponentSetException(String message) {
		super(message);
	}

	public PcpComponentSetException(Throwable cause) {
		super(cause);
	}

}
