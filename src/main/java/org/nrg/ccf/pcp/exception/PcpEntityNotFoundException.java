package org.nrg.ccf.pcp.exception;

public class PcpEntityNotFoundException extends PcpException {

	private static final long serialVersionUID = 6730341569388865562L;

	public PcpEntityNotFoundException() {
		super();
	}

	public PcpEntityNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PcpEntityNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public PcpEntityNotFoundException(String message) {
		super(message);
	}

	public PcpEntityNotFoundException(Throwable cause) {
		super(cause);
	}

}
