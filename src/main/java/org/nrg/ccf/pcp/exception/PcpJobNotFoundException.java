package org.nrg.ccf.pcp.exception;

public class PcpJobNotFoundException extends Exception {

	private static final long serialVersionUID = -2859475349045571994L;

	public PcpJobNotFoundException() {
		super();
	}

	public PcpJobNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PcpJobNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public PcpJobNotFoundException(String message) {
		super(message);
	}

	public PcpJobNotFoundException(Throwable cause) {
		super(cause);
	}

}
