package org.nrg.ccf.pcp.preferences;

import java.util.List;

import org.nrg.framework.constants.Scope;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.beans.AbstractPreferenceBean;
import org.nrg.prefs.entities.Preference;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.exceptions.UnknownToolId;
import org.nrg.prefs.services.NrgPreferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@NrgPreferenceBean(toolId = "ccfSessionBuilding", toolName = "CCF Session Building Plugin Preferences",
			description = "Manages CCF session building resources plugin preferences", strict = true)
public class PcpPreferences extends AbstractPreferenceBean {
	
	private static final long serialVersionUID = 6882478212024940864L;
	private static final Logger _logger = LoggerFactory.getLogger(PcpPreferences.class);
	public static final String PCP_ENABLED = "pcpEnabled";
	public static final Scope SCOPE = Scope.Project;
	private static final String _pcpEnabledProjectsQuery = "SELECT DISTINCT entity_id FROM xhbm_preference WHERE name = '" + PCP_ENABLED + "' and value='true'";
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public PcpPreferences(NrgPreferenceService preferenceService, JdbcTemplate jdbcTemplate) {
		super(preferenceService);
		this.jdbcTemplate = jdbcTemplate;
	}
	
	// Make PreferenceBeanHelper happy.  It wants the annotation to be on a getter with no arguments.
	@NrgPreference
	public Boolean getPcpEnabled() {
		return null;
	}
	
	public Boolean getPcpEnabled(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, PCP_ENABLED);
	}
	
	public void setPcpEnabled(final String entityId, final boolean enabled) {
		// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
		// time permits, that bug should be addressed.
		removeSiteLevelPreferenceIfExists(PCP_ENABLED);
		if (entityId==null || entityId.length()<1) {
			// Don't set value.  This is a project preference.
			_logger.debug("PCP Enabled preference - empty/null entity value passed");
			return;
		}
		try {
			_logger.debug("PCP Enabled preference - Setting value for entity=" + entityId);
			this.setBooleanValue(SCOPE, entityId, enabled, PCP_ENABLED);
			if (sitePreferenceExists(PCP_ENABLED) != null) {
				throw new RuntimeException("EXCEPTION:  Preference may not have been created.  Please try again.");
			}
		} catch (UnknownToolId | InvalidPreferenceName e) {
			_logger.error("Exception setting pipeline control panel enabled preference", e);
		}
	}
	
	// TODO:  Workaround method (XNAT-5134)
	private void removeSiteLevelPreferenceIfExists(String key) {
		try {
			if (sitePreferenceExists(key) != null) {
				this.delete(Scope.Site,"",key);
			}
		} catch (InvalidPreferenceName e) {
			// Do nothing.
		}
	}
	// TODO:  Workaround method (XNAT-5134)
	private Preference sitePreferenceExists(String key) {
		Preference p = this.getPreference(Scope.Site, "", key);
		if (p != null) {
			_logger.error("ERROR:  Found site level preference where only project preferences should exist (KEY=" + key + ").");
			
		}
		return p;
	}

	public List<String> getPcpEnabledProjects() {
		return jdbcTemplate.queryForList(_pcpEnabledProjectsQuery, String.class);
	}
	
	
}
