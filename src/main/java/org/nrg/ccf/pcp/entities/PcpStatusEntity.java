package org.nrg.ccf.pcp.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.ccf.pcp.dto.PcpCondensedStatus;
import org.nrg.ccf.pcp.dto.PcpProjectPipelineEntity;
import org.nrg.ccf.pcp.dto.PcpProjectPipelineSubgroupEntity;
import org.nrg.ccf.pcp.dto.PcpStatusEntityT;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PcpStatusEntityI;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
@Table(uniqueConstraints = {
		// TODO:  To we want to require specification of XSITYPE here?
		//@UniqueConstraint(columnNames = {"project", "pipeline", "entityType", "entityLabel", "subGroup"}),
		@UniqueConstraint(columnNames = {"project", "pipeline", "entityLabel", "subGroup"}),
		@UniqueConstraint(columnNames = {"project", "pipeline", "entityId", "subGroup"})
		})
public class PcpStatusEntity extends AbstractHibernateEntity implements PcpStatusEntityI, PcpCondensedStatusI {

	private static final long serialVersionUID = -279834743572383911L;

	//public static final int MAX_FIELD_LENGTH = 10485760;
	public static final int FIELD_LENGTH = 163840;

	private String project = "";
	private String pipeline = "";
	private String entityType = "";
	private String entityId = "";
	private String entityLabel = "";
	private String subGroup = "";
	private Date submitTime = new Date(0);
	private String submitNode = "";
	private String submitExecManager = "";
	
	private String status = "";
	private Date statusTime = new Date();
	private String statusInfo = "";
	
	private Boolean prereqs = false;
	private Date prereqsTime = new Date(0);
	private String prereqsInfo = "";
	
	private Boolean validated = false;
	private Date validatedTime = new Date(0);
	private String validatedInfo = "";
	
	private Boolean issues = false;
	private Date issuesTime = new Date(0);
	private String issuesInfo = "";
	
	private Boolean impeded = false;
	private Date impededTime = new Date(0);
	private String impededInfo = "";
	
	private String notes = "";
	
	public PcpStatusEntity() {
		super();
	}
	
	public PcpStatusEntity(final PcpStatusEntityT statusEntityT) {
		this.project=statusEntityT.getProject();
		this.pipeline=statusEntityT.getPipeline();
		this.entityType=statusEntityT.getEntityType();
		this.entityId=statusEntityT.getEntityId();
		this.entityLabel=statusEntityT.getEntityLabel();
		this.subGroup=statusEntityT.getSubGroup();
		this.submitTime=statusEntityT.getSubmitTime();
		this.submitNode=statusEntityT.getSubmitNode();
		this.submitExecManager=statusEntityT.getSubmitExecManager();
		this.statusInfo=statusEntityT.getStatusInfo();
		this.prereqs=statusEntityT.getPrereqs();
		this.prereqsTime=statusEntityT.getPrereqsTime();
		this.prereqsInfo=statusEntityT.getPrereqsInfo();
		this.validated=statusEntityT.getValidated();
		this.validatedTime=statusEntityT.getValidatedTime();
		this.validatedInfo=statusEntityT.getValidatedInfo();
		this.issues=statusEntityT.getIssues();
		this.issuesTime=statusEntityT.getIssuesTime();
		this.issuesInfo=statusEntityT.getIssuesInfo();
		this.impeded=statusEntityT.getImpeded();
		this.impededTime=statusEntityT.getImpededTime();
		this.impededInfo=statusEntityT.getImpededInfo();
	}

	@Column(nullable=false)
	@Override
	public String getProject() {
		return project;
	}

	@Override
	public void setProject(String project) {
		this.project = project;
	}

	@Column(nullable=false)
	@Override
	public String getPipeline() {
		return pipeline;
	}

	@Override
	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}
	
	@Column(nullable=false)
	@Override
	public String getEntityType() {
		return entityType;
	}

	@Override
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
	@Column(nullable=false)
	@Override
	public String getEntityId() {
		return entityId;
	}

	@Override
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	@Column(nullable=false)
	@Override
	public String getEntityLabel() {
		return entityLabel;
	}

	@Override
	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}

	@Column(nullable=false)
	@Override
	public String getSubGroup() {
		return subGroup;
	}

	@Override
	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}

	@Override
	public Date getSubmitTime() {
		return submitTime;
	}

	@Override
	public void setSubmitTime(Date submitTime) {
		this.submitTime = submitTime;
	}

	@Override
	public String getSubmitNode() {
		return submitNode;
	}

	@Override
	public void setSubmitNode(String submitNode) {
		this.submitNode = submitNode;
	}

	@Override
	public String getSubmitExecManager() {
		return submitExecManager;
	}

	@Override
	public void setSubmitExecManager(String submitExecManager) {
		this.submitExecManager = submitExecManager;
	}

	@Override
	public String getStatus() {
		return status;
	}

	@Override
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public Date getStatusTime() {
		return statusTime;
	}

	@Override
	public void setStatusTime(Date statusTime) {
		this.statusTime = statusTime;
	}

	@Column(length=FIELD_LENGTH)
	public String getStatusInfo() {
		return statusInfo;
	}

	@Override
	public void setStatusInfo(String statusInfo) {
		this.statusInfo = statusInfo;
	}

	@Override
	public Boolean getPrereqs() {
		return prereqs;
	}

	@Override
	public void setPrereqs(Boolean prereqs) {
		this.prereqs = prereqs;
	}

	@Override
	public Date getPrereqsTime() {
		return prereqsTime;
	}

	@Override
	public void setPrereqsTime(Date prereqsTime) {
		this.prereqsTime = prereqsTime;
	}

	@Column(length=FIELD_LENGTH)
	@Override
	public String getPrereqsInfo() {
		return prereqsInfo;
	}

	@Override
	public void setPrereqsInfo(String prereqsInfo) {
		this.prereqsInfo = prereqsInfo;
	}

	@Override
	public Boolean getValidated() {
		return validated;
	}

	@Override
	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	@Override
	public Date getValidatedTime() {
		return validatedTime;
	}

	@Override
	public void setValidatedTime(Date validatedTime) {
		this.validatedTime = validatedTime;
	}

	@Column(length=FIELD_LENGTH)
	@Override
	public String getValidatedInfo() {
		return validatedInfo;
	}

	@Override
	public void setValidatedInfo(String validatedInfo) {
		this.validatedInfo = validatedInfo;
	}

	@Override
	public Boolean getIssues() {
		return issues;
	}

	@Override
	public void setIssues(Boolean issues) {
		this.issues = issues;
	}

	@Override
	public Date getIssuesTime() {
		return issuesTime;
	}

	@Override
	public void setIssuesTime(Date issuesTime) {
		this.issuesTime = issuesTime;
	}

	@Column(length=FIELD_LENGTH)
	@Override
	public String getIssuesInfo() {
		return issuesInfo;
	}

	@Override
	public void setIssuesInfo(String issuesInfo) {
		this.issuesInfo = issuesInfo;
	}

	@Override
	public Boolean getImpeded() {
		return impeded;
	}

	@Override
	public void setImpeded(Boolean impeded) {
		this.impeded = impeded;
	}

	@Override
	public Date getImpededTime() {
		return impededTime;
	}

	@Override
	public void setImpededTime(Date impededTime) {
		this.impededTime = impededTime;
	}

	@Column(length=FIELD_LENGTH)
	@Override
	public String getImpededInfo() {
		return impededInfo;
	}

	@Override
	public void setImpededInfo(String impededInfo) {
		this.impededInfo = impededInfo;
	}

	@Column(length=FIELD_LENGTH)
	@Override
	public String getNotes() {
		return notes;
	}

	@Override
	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Transient
	public PcpCondensedStatusI getCondensedStatus() {
		return new PcpCondensedStatus(this);
	}

	@Transient
	public PcpStatusEntityT getTransferEntity() {
		return new PcpStatusEntityT(this);
	}
	
	@Override
	@Transient
	public String toString() {
		return "PcpStatusEntity - (PROJECT=" + project + ", PIPELINE=" + pipeline + ", ENTITY=" + entityLabel + ", SUBGROUP=" + subGroup + ")";
	}

	@Override
	@Transient
	public int compareTo(final PcpCondensedStatusI status) {
		int cmp = project.compareTo(status.getProject());
		if (cmp != 0) return cmp;
		cmp = pipeline.compareTo(status.getPipeline());
		if (cmp != 0) return cmp;
		cmp = entityLabel.compareTo(status.getEntityLabel());
		if (cmp != 0) return cmp;
		cmp = subGroup.compareTo(status.getSubGroup());
		return cmp;
	}

	@Transient
	public PcpProjectPipelineEntity getProjectPipelineEntity() {
		return new PcpProjectPipelineEntity(project,pipeline,entityId);
	}

	@Transient
	public PcpProjectPipelineSubgroupEntity getProjectPipelineSubgroupEntity() {
		return new PcpProjectPipelineSubgroupEntity(project,pipeline,subGroup,entityId);
	}

}
