package org.nrg.ccf.pcp.anno.processors;

import org.kohsuke.MetaInfServices;
import com.google.common.collect.Maps;

import org.nrg.ccf.pcp.anno.PipelineExecManager;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.pcp.anno.PipelineExecManager")
public class PipelineExecManagerProcessor extends NrgAbstractAnnotationProcessor<PipelineExecManager> {
	
	@Override
	protected Map<String, String> processAnnotation(TypeElement element, PipelineExecManager annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(PipelineExecManager.PIPELINE_EXEC_MANAGER, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, PipelineExecManager annotation) {
        return String.format("pcp/%s-pcp.properties", element.getSimpleName());
	}

}
