package org.nrg.ccf.pcp.inter;

import java.util.List;

public interface ConfigurableComponentI {

	List<String> getConfigurationYaml();

}
