package org.nrg.ccf.pcp.inter;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.exception.PcpSubmitException;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xft.security.UserI;

public interface PipelineSubmitterI {

	List<String> getParametersYaml(String projectId, String pipelineId);

	boolean submitJob(PcpStatusEntity entity, PcpStatusEntityService entityService, PipelineValidatorI validator, Map<String, String> parameters, UserI user) throws PcpSubmitException;

}
