package org.nrg.ccf.pcp.inter;

import org.nrg.xft.security.UserI;
import java.util.List;
import java.util.Map;

public interface PipelineExecManagerI {
	
	void submitJobs(PipelineSubmitterI submitter, PipelineValidatorI validator, List<PcpCondensedStatusI> statusList, Map<String, String> parameters, UserI user);

}
