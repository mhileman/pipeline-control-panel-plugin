package org.nrg.ccf.pcp.inter;

import java.util.List;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.ccf.pcp.services.PcpStatusEntityService;
import org.nrg.xft.security.UserI;

public interface PipelineSelectorI {
	
	List<PcpStatusEntity> getStatusEntities(PcpStatusEntityService _statusEntityService, String projectId, String pipelineId, UserI user);

}
