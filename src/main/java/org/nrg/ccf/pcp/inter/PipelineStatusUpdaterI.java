package org.nrg.ccf.pcp.inter;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xft.security.UserI;

public interface PipelineStatusUpdaterI {
	
	void updateStatus(PcpStatusEntity statusEntity, UserI user);

}
