package org.nrg.ccf.pcp.inter;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xft.security.UserI;

public interface PipelineValidatorI {

	void validate(PcpStatusEntity statusEntity, UserI user);

}
