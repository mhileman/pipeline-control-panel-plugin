package org.nrg.ccf.pcp.runner;

import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineExecManagerI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.xft.security.UserI;

public class PcpPipelineRunner implements Runnable {

	private final PipelineExecManagerI _execManager;
	private final PipelineSubmitterI _submitter;
	private final PipelineValidatorI _validator;
	private final List<PcpCondensedStatusI> _statusList;
	private final Map<String, String> _parameters;
	private UserI _user;

	public PcpPipelineRunner(PipelineExecManagerI execManager, PipelineSubmitterI submitter, PipelineValidatorI validator,
			List<PcpCondensedStatusI> statusList, Map<String, String> parameters, UserI user) {
		_execManager = execManager;
		_submitter = submitter;
		_statusList = statusList;
		_parameters = parameters;
		_validator = validator;
		_user = user;
	}

	@Override
	public void run() {
		_execManager.submitJobs(_submitter, _validator, _statusList, _parameters, _user);
	}

}
