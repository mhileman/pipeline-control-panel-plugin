package org.nrg.ccf.pcp.queue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.pcp.dto.PcpProjectPipeline;
import org.nrg.ccf.pcp.dto.PcpSubmitStatus;
import org.nrg.ccf.pcp.exception.PcpJobNotFoundException;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PcpSubmitQueue {
	
	private static final Logger _logger = LoggerFactory.getLogger(PcpSubmitQueue.class);
	final static List<Long> _overallList = new ArrayList<>();
	final static List<Long> _cancelledList = new ArrayList<>();
	final static Map<PcpProjectPipeline, List<Long>> _runningMap = new HashMap<>();
	final static Map<PcpProjectPipeline, List<Long>> _waitingMap = new HashMap<>();
	final static Map<PcpProjectPipeline, Integer> _failedMap = new HashMap<>();
	final static Map<Long, List<PcpCondensedStatusI>> _jobInfoMap = new HashMap<>();
	// TODO Make this configurable?
	public static final int max_pipeline_concurrent_runs = 1;
	public static final int max_concurrent_runs = 4;
	

	public static synchronized boolean mayProceed(PcpQueueMonitor pcpQueueMonitor) {
		final PcpProjectPipeline pp = pcpQueueMonitor.getPcpProjectPipeline();
		//_logger.debug("call mayProceed:  " + pcpQueueMonitor.getId());
		final Long id = pcpQueueMonitor.getId();
		if (!_runningMap.containsKey(pp)) {
			_runningMap.put(pp,  new ArrayList<Long>());
		}
		if (!_waitingMap.containsKey(pp)) {
			_waitingMap.put(pp,  new ArrayList<Long>());
		}
		if (!_failedMap.containsKey(pp)) {
			_failedMap.put(pp,  Integer.valueOf(0));
		}
		final List<Long> runList = _runningMap.get(pp);
		final List<Long> waitList = _waitingMap.get(pp);
		Integer failedCount = _failedMap.get(pp);
		final int overallSize = _overallList.size();
		final int runSize = runList.size();
		final int waitSize = waitList.size();
		if (!_jobInfoMap.containsKey(id)) {
			_jobInfoMap.put(id, pcpQueueMonitor.getSubmitList());
		}
		if (runSize<max_pipeline_concurrent_runs && overallSize<max_concurrent_runs && waitSize<1) {
			runList.add(id);
			_overallList.add(id);
			//_logger.debug("call mayProceed TRUE(1):  " + pcpQueueMonitor.getId());
			return true;
		} else if (runSize<max_pipeline_concurrent_runs && overallSize<max_concurrent_runs && waitList.contains(id) && waitList.get(0).equals(id)) {
			waitList.remove(id);
			runList.add(id);
			_overallList.add(id);
			//_logger.debug("call mayProceed TRUE(2):  " + pcpQueueMonitor.getId());
			return true;
		} else if ((runSize>=max_pipeline_concurrent_runs || overallSize>=max_concurrent_runs) && !waitList.contains(id)) {
			waitList.add(id);
			//_logger.debug("call mayProceed FALSE(3):  " + pcpQueueMonitor.getId());
			return false;
		} else if ((runSize>=max_pipeline_concurrent_runs || overallSize>=max_concurrent_runs) && waitList.contains(id)) {
			//_logger.debug("call mayProceed FALSE(4):  " + pcpQueueMonitor.getId());
			return false;
		} else if (runSize<max_pipeline_concurrent_runs && overallSize<max_concurrent_runs && waitList.contains(id) && failedCount>waitSize) {
			runList.add(id);
			_overallList.add(id);
			waitList.remove(id);
			failedCount = 0;
			//_logger.debug("call mayProceed TRUE(5):  " + pcpQueueMonitor.getId());
			return true;
		} else if (runSize<max_pipeline_concurrent_runs && overallSize<max_concurrent_runs && waitList.contains(id) && failedCount<=waitSize) {
			failedCount++;
			//_logger.debug("call mayProceed FALSE(6):  " + pcpQueueMonitor.getId());
			return false;
		} else {
			_logger.error("Uh-oh!  PcpSubmitQueue mayProceed is not considering all queuing states! (id=" + id + ", failedCount=" + failedCount +
					", runList=" + runList + ", waitList=" + waitList + ", ProjectPipeline=" + pp);
			return false;
		}
	}

	public static synchronized boolean isCancelled(PcpQueueMonitor pcpQueueMonitor) {
		final Long id = pcpQueueMonitor.getId();
		//_logger.debug("call isCancelled:  " + pcpQueueMonitor.getId());
		if (!_cancelledList.contains(id)) {
			//_logger.debug("call isCancelled FALSE(1):  " + pcpQueueMonitor.getId());
			return false;
		}
		final PcpProjectPipeline pp = pcpQueueMonitor.getPcpProjectPipeline();
		final List<Long> waitList = _waitingMap.get(pp);
		if (!waitList.contains(id)) {
			//_logger.debug("call isCancelled FALSE(2):  " + pcpQueueMonitor.getId());
			return false;
		}
		waitList.remove(id);
		_jobInfoMap.remove(id);
		_cancelledList.remove(id);
		//_logger.debug("call isCancelled TRUE(3):  " + pcpQueueMonitor.getId());
		return true;
	}

	public static synchronized void cancel(String projectId, Long id) throws PcpJobNotFoundException {
		if (!_jobInfoMap.containsKey(id)) {
			throw new PcpJobNotFoundException("Job not found for jobid=" + id + ".");
		}
		final List<PcpCondensedStatusI> statusList = _jobInfoMap.get(id);
		if (statusList.isEmpty() || !statusList.get(0).getProject().equals(projectId)) {
			throw new PcpJobNotFoundException("Requested job id does not belong to this project (JOBID=" + id + ").");
		}
		_cancelledList.add(id);
	}

	public static synchronized void finished(PcpQueueMonitor pcpQueueMonitor) {
		//_logger.debug("call Finished:  " + pcpQueueMonitor.getId());
		final PcpProjectPipeline pp = pcpQueueMonitor.getPcpProjectPipeline();
		final Long id = pcpQueueMonitor.getId();
		final List<Long> runList = _runningMap.get(pp);
		final List<Long> waitList = _runningMap.get(pp);
		runList.remove(id);
		_overallList.remove(id);
		// Should only contain this object in the unlikely event that the sleep thread was interrupted
		waitList.remove(id);
		_jobInfoMap.remove(id);
		//_logger.debug("call Finished VOID(1):  " + pcpQueueMonitor.getId());
	}

	public static synchronized PcpSubmitStatus getPcpSubmitStatus(final String projectId) {
		final PcpSubmitStatus status = new PcpSubmitStatus();
		status.setTotalRunningJobs(_overallList.size());
		final Map<String,Map<Integer, List<PcpCondensedStatusI>>> runningJobs = new HashMap<>();
		for (final PcpProjectPipeline pp : _runningMap.keySet()) {
			if (pp.getProject().equals(projectId)) {
				final String pipeline = pp.getPipeline();
				if (!runningJobs.containsKey(pipeline)) {
					runningJobs.put(pipeline, new HashMap<Integer, List<PcpCondensedStatusI>>());
				}
				final Map rpMap = runningJobs.get(pipeline);
				final List<Long> ppList = _runningMap.get(pp);
				for (final Long ppId : ppList) {
					rpMap.put(ppId,_jobInfoMap.get(ppId));
				}
			}
		}
		final Map<String,Map<Integer, List<PcpCondensedStatusI>>> waitingJobs = new HashMap<>();
		for (final PcpProjectPipeline pp : _waitingMap.keySet()) {
			if (pp.getProject().equals(projectId)) {
				final String pipeline = pp.getPipeline();
				if (!waitingJobs.containsKey(pipeline)) {
					waitingJobs.put(pipeline, new HashMap<Integer, List<PcpCondensedStatusI>>());
				}
				final Map rpMap = waitingJobs.get(pipeline);
				final List<Long> ppList = _waitingMap.get(pp);
				for (final Long ppId : ppList) {
					rpMap.put(ppId,_jobInfoMap.get(ppId));
				}
			}
		}
		status.setRunningJobs(runningJobs);
		status.setWaitingJobs(waitingJobs);
		return status;
	}

}
