package org.nrg.ccf.pcp.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "pipelineControlPanelPlugin",
			name = "Pipeline Control Panel Plugin",
			entityPackages = "org.nrg.ccf.pcp.entities" 
		)
@ComponentScan({ 
		"org.nrg.ccf.pcp.conf",
		"org.nrg.ccf.pcp.dao",
		"org.nrg.ccf.pcp.xapi",
		"org.nrg.ccf.pcp.scheduler",
		"org.nrg.ccf.pcp.services",
		"org.nrg.ccf.pcp.preferences",
		"org.nrg.ccf.pcp.utils"
	})
public class PipelineControlPanelPlugin {
	
	public static Logger logger = Logger.getLogger(PipelineControlPanelPlugin.class);

	public PipelineControlPanelPlugin() {
		logger.info("Configuring the pipeline control panel plugin.");
	}
	
}