if (typeof CCF === 'undefined') {
  CCF = {};
}
if (typeof CCF.pcp === 'undefined') {
  CCF.pcp = {
    PREF_URL: serverRoot+"/xapi/pipelineControlPanelConfig/"+XNAT.data.context.project+"/settings/",
    project: XNAT.data.context.project
  };
}

CCF.pcp.initialize = function(projectId) {
  CCF.pcp.project = projectId

  $.ajax({
    type: "GET",
    url:CCF.pcp.PREF_URL,
    cache: false,
    async: true,
    context: this,
    dataType: 'json'
  }).done( function(data, textStatus, jqXHR) {
    if ('pcpEnabled' in data) {
      if (data.pcpEnabled) {
        $(".pcpLink").show();
      }
    }
  }).fail( function(data, textStatus, jqXHR) {
    console.log("ERROR:  Error returning Pipeline Control Panel settings.");
  });
}

CCF.pcp.renderPipelineControlPanel = function() {
  var getSearchParams = function(k){
     var p={};
     location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
     return k?p[k]:p;
  }
  CCF.pcp.project = getSearchParams("project")
  CCF.pcp.pipeline = getSearchParams("pipeline")

  if (CCF.pcp.pipeline) {
    CCF.pcp.renderPipelineControls()
    CCF.pcp.renderPipelineTable()
  } else {
    CCF.pcp.renderOverviewControls()
    CCF.pcp.renderOverviewTable()
  }
}

CCF.pcp.renderBreadcrumbs = function() {
	 // Hide XNAT breadcrumbs and create custom
  $('a[id^="breadcrumb"]').hide()
  $('#project-link').text(CCF.pcp.project)
  $('#project-link').attr('href','/data/projects/'+CCF.pcp.project)
  $('#pcp-overview-link').attr('href','/app/template/Page.vm?view=pcp&project='+CCF.pcp.project)
}

CCF.pcp.renderOverviewControls = function() {
	var $pcpControls = $('#pcp-controls-container')
  $pcpControls.empty()

  CCF.pcp.renderBreadcrumbs()

  var refreshAction = "CCF.pcp.renderOverviewTable()"
  var refreshButton = '<button class="btn" onclick=' + refreshAction + '>Refresh</button>&nbsp;'
  var settingsButton = '<button class="btn" onclick="location.href=\'/app/template/Page.vm?view=project/settings&id=' + CCF.pcp.project + '\'">Settings</button>&nbsp;'
  $pcpControls.append(refreshButton)
  $pcpControls.append(settingsButton)
}

CCF.pcp.renderPipelineControls = function() {
	var $pcpControls = $('#pcp-controls-container')
	$pcpControls.empty()

	CCF.pcp.renderBreadcrumbs()

  $('#pcp-title').text(CCF.pcp.pipeline)

  // Need to add tooltip to explain the difference once these are spawner elements
  var quickRefreshButton = '<button class="btn" onclick="CCF.pcp.renderPipelineTable(true)">Refresh</button>&nbsp;'
  //var fullRefreshButton = '<button class="btn" onclick="CCF.pcp.renderPipelineTable(false)">Update Cache</button>&nbsp;'
  var fullRefreshButton = '<button class="btn" onclick="CCF.pcp.renderStatusUpdate()">Update Cache</button>&nbsp;'
  var exportButton = '<button>Export</button>'
  var submitButton = '<button class="submit btn" onclick="CCF.pcp.renderSubmitParameters()">Launch Pipeline</button>'
  $pcpControls.append(quickRefreshButton)
  $pcpControls.append(fullRefreshButton)
  $pcpControls.append(submitButton)
}

CCF.pcp.renderOverviewTable = function() {
  $('#pcp-table-container').empty()

  if (typeof CCF.pcp.project === 'undefined' || CCF.pcp.project == "") {
     var queryParams = new URLSearchParams(window.location.search)
     CCF.pcp.project = queryParams.get("project")
     CCF.pcp.pipeline = queryParams.get("pipeline")
  }

  XNAT.table.dataTable([], {
     url: '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/statusSummary?includeSubgroupSummary=false',
     table: {
        id: 'pcp-overview',
        className: 'pcp-table'
     },
     columns: {
        pipeline: {
          label: 'Pipeline',
          filter: true,
          apply: function(pipeline) {
            // var actions = "CCF.pcp.renderPipelineControls('"+pipeline+"');CCF.pcp.renderPipelineTable()"
            // return '<a onclick="' + actions + '">' + pipeline + '</a>'
            return '<a href=/app/template/Page.vm?view=pcp&project='+CCF.pcp.project+'&pipeline='+pipeline+'>'+pipeline+'</a>'
          }
        },
        notReady: {
          label: 'Not Ready',
          sort: true
        },
        ready: {
          label: 'Ready',
          sort: true
        },
        running: {
          label: 'Queued or Running',
          sort: true
        },
        issues: {
          label: 'Issues',
          sort: true
        },
        complete: {
          label: 'Complete',
          sort: true
        },
        total: {
          label: 'Total',
          sort: true
        }
     }
  }).render($('#pcp-table-container'))
}

CCF.pcp.renderPipelineTable = function(cached=true) {
  CCF.pcp.allStatusEntities = []

  $('#pcp-table-container').empty()
  var loadingMsg = "Loading cached table"
  if (!cached) {
    loadingMsg = "Updating control panel cache..."
    try {
      xmodal.loading.open(loadingMsg)
    } catch(err) {
      console.log("xmodal.loading.open failed")
    }
  }
  // xmodal doesn't seem to be loaded at this point for whatever reason
  // dialog.loading works, but can't close since there's no dataTable.done() method
  // xmodal.loading.open(loadingMsg)
  // XNAT.ui.dialog.loading.open()

  var url = serverRoot + '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
     '/pipeline/' + CCF.pcp.pipeline +
     '/status?cached=' + cached.toString() + '&condensed=false';

  var status_update_url = serverRoot + '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
     '/pipeline/' + CCF.pcp.pipeline +
     '/status?cached=' + cached.toString() + '&condensed=false';

  var tabindex = 0;

  XNAT.xhr.get({
  	url: url,
	cache: false,
	async: true,
	context: this,
  }).done( function(data, textStatus, jqXHR) {


      if (jqXHR.status == 202) {
          msg = "<b>The update process has returned early because it is expected to take quite some time to run. " +
                " Only some of the rows have completed processing.</b><br><br>The process will keep running and " +
		"you may refresh or check this page again " +
                " later to see the final results.";
          xmodal.message({ title: "Incomplete",
			width: '500px',
			height: '300px',
			content: msg
		});
      }
      if (data.length<1) {
	  	$('#pcp-table-container').html("<div style='margin-left:10px;margin-right:80px;margin-top:10px;max-width:1000px;'><h2>No pipeline control panel data for this pipeline</h2>" +
			"This may be because this pipeline was recently configured and the status update job has not yet run.  You may click " +
  			'<button class="btn" onclick="CCF.pcp.renderPipelineTable(false)">HERE</button> to request a status update run and refresh of this page, ' +
			"however in many cases a full status update run takes quite some time to run." +  
			"</div>");
		return;
      }

      XNAT.table.dataTable(data, {
        //url: url,
        table: {
          id: 'pcp-' + CCF.pcp.pipeline,
          className: 'pcp-table highlight'
        },
        columns: {
          run: {
            label: '<input id="select-all" name="select-all" type="checkbox">',
            td: {'className': 'center'},
            apply: function() {
    
              CCF.pcp.allStatusEntities.push(this)
              tabindex++
    
              return (
                '<input id="' + this.entityLabel + '-' + this.subGroup + '" ' +
                        'name="entity-check" ' +
                        'class="chkbox" ' +
                        'type="checkbox" ' +
                        'tabindex="'+tabindex+'">'
              )
            }
          },
          entityLabel: {
            label: 'Entity',
            filter: true,
            sort: true,
            td: {'className': 'entityLabel center'},
            apply: function(entity) {
              var html = entity
              if (this.entityType === "xnat:subjectData") {
                href = '/data/projects/'+CCF.pcp.project+'/subjects/'+entity+'?format=html'
                html = '<a href='+href+' target="_blank" >' + entity + '</a>'
              } else if (this.entityType == "xnat:mrSessionData") {
                href = '/data/projects/'+CCF.pcp.project+'/experiments/'+entity+'?format=html'
                html = '<a href='+href+' target="_blank" >' + entity + '</a>'
              }
              return html
            }
          },
          subGroup: {
            label: 'Subgroup',
            filter: true,
            sort: true,
            td: {'className': 'subGroup center'},
          },
          status: {
            label: 'Status',
            sort: true,
            filter: true,
            td: {'className': 'status center'},
            apply: function(status) {
              var args = [this.entityLabel, this.subGroup]
              var argsStr = "'" + args.join("','") + "'"
    
              var disabled = ""
              if (!this.statusInfo) {
                disabled = "disabled"
              }
    
              var statusIcons = {
                'RUNNING': '<i class="fa fa-spinner fa-spin">',
                'COMPLETE': '<i class="fa fa-check">',
                'EXT_COMPLETE': '<i class="fa fa-check">',
                'ERROR': '<i class="fa fa-times">',
                'SUBMITTED': '<i class="fa fa-ellipsis-h">',
                'NOT_SUBMITTED': '<i class="fa fa-minus">'
              }
              try {
              	var icon = statusIcons[status]
              } catch(err) {
              	var icon = ""
              }
    
              return (
                '<button class="btn btn-sm" style="width:150px" ' + disabled +
                  ' onclick="CCF.pcp.showStatusInfo('+argsStr+')">' +
                   status + " " + icon +
                '</button>'
              )
            }
          },
          prereqs: {
            label: 'Prereqs Met',
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '1' },
                  { label: 'No', value: '0' }
                ],
                element: filterMenuElement.call(table, 'prereqs')
              }).element])
            },
            td: {'className': 'prereqs center'},
            apply: function(prereqs) {
              return CCF.pcp.generateInfoButton(
                this.entityLabel, this.subGroup, prereqs, this.prereqsInfo, 'prereqs')
            }
          },
          validated: {
            label: 'Resources Validated',
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '1' },
                  { label: 'No', value: '0' }
                ],
                element: filterMenuElement.call(table, 'validated')
              }).element])
            },
            td: {'className': 'validated center'},
            apply: function(validated) {
              return CCF.pcp.generateInfoButton(
                this.entityLabel, this.subGroup, validated, this.validatedInfo, 'validated')
            }
          },
          issues: {
            label: 'Issues',
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '0' },
                  { label: 'No', value: '1' }
                ],
                element: filterMenuElement.call(table, 'issues')
              }).element])
            },
            td: {'className': 'issues center'},
            apply: function(issues) {
              return CCF.pcp.generateEditableInfoButton(
                this.entityLabel, this.subGroup, !issues, 'issues')
            }
          },
          impeded: {
            label: "Runnable",
            filter: function(table) {
              return spawn('div.center', [XNAT.ui.select.menu({
                value: 'all',
                options: [
                  { label: 'All', value: 'all' },
                  { label: 'Yes', value: '1' },
                  { label: 'No', value: '0' }
                ],
                element: filterMenuElement.call(table, 'impeded')
              }).element])
            },
            td: {'className': 'impeded center'},
            apply: function(impeded) {
              return CCF.pcp.generateEditableInfoButton(
                this.entityLabel, this.subGroup, !impeded, 'impeded')
            }
          },
          notes: {
            label: 'Notes',
            filter: true,
            td: {'className': 'notes center'},
            apply: function(notes) {
              var icon = '<i class="fa fa-comment-o" aria-hidden="true"></i>'
              if (notes) {
                icon = '<i class="fa fa-commenting-o" aria-hidden="true"></i>'
              }
    
              var args = [this.entityLabel, this.subGroup]
              var argsStr = "'" + args.join("','") + "'"
    
              return (
                '<button class="btn btn-sm" ' +
                  'onclick="CCF.pcp.showNotes('+argsStr+')">' + icon +
                '</button>'
               )
            }
          }
        }
      }).render($('#pcp-table-container'))

      initSelectAll();
      initShiftSelect();

  }).fail( function(data, textStatus, jqXHR) {
		xmodal.message("Error","ERROR:  Could not build the pipeline control panel table.");
  });

}

function initSelectAll() {
  $('#select-all').click(function(event) {
    if(this.checked) {
      $(':checkbox').each(function() {
        // Make sure it's not a hidden/filtered row
        var $row = $(this).closest('tr')
        // console.log($row.css("display"))
        if ($row.css("display") === "table-row") {
          this.checked = true;
        }
      });
    } else {
      $(':checkbox').each(function() {
        this.checked = false;
      });
    }
  });

  // Disable this here for now
  // Eventually goes away once custom filter implemented
  $("input[title='notes:filter']").attr('disabled', 'disabled')
  $("input[title='notes:filter']").attr('placeholder', 'Not implemented')
}

function initShiftSelect() {
  var lastChecked = null;
  var $chkboxes = $('.chkbox')

  $chkboxes.click(function(e) {
      if(!lastChecked) {
          lastChecked = this
          return
      }
      if(e.shiftKey) {
        var start = $chkboxes.index(this)
        var end = $chkboxes.index(lastChecked)

        $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked)
      }
      lastChecked = this
  });
}

// set up custom filter menus
function filterMenuElement(prop){
  if (!prop) return false;
  // call this function in context of the table
  var $pipelineTable = $(this);
  var FILTERCLASS = 'filter-' + prop;
  return {
    id: 'pcp-filter-select-' + prop,
    on: {
      change: function() {
        var selectedValue = $(this).val();
        // console.log(selectedValue);

        $pipelineTable.find('button[class*="filter-'+prop+'"]').each(function() {
          var $row = $(this).closest('tr');
          // console.log($row)
          if (selectedValue === 'all') {
            $row.removeClass(FILTERCLASS);
            return;
          }

          $row.addClass(FILTERCLASS);
          // if (selectedValue == this.textContent) {
          if (this.textContent.includes(selectedValue)) {
            $row.removeClass(FILTERCLASS);
          }
        })
      }
    }
  };
}

CCF.pcp.generateInfoButton = function(entity, group, successful, infoText, key) {
  var icon = '<i class="fa fa-times" style="color:red"></i>'
  var filterVal = 0
  if (successful) {
    icon = '<i class="fa fa-check" style="color:green"></i>'
    filterVal = 1
  }

  var args = [entity, group, key]
  var argsStr = "'" + args.join("','") + "'"
  // var disabled = "disabled" ? infoText: ""
  var disabled = ""
  if (!infoText) {
    disabled = "disabled"
  }

  return (
    '<button class="btn btn-sm filter-'+key+'" ' + disabled +
      ' onclick="CCF.pcp.showEntityInfo('+argsStr+')">' + icon + ' ' +
      ' <i class="hidden sorting filtering '+key+'">'+filterVal+'</i>' +
    '</button>'
  )
}

CCF.pcp.generateEditableInfoButton = function(entity, group, successful, key) {
  var icon = '<i class="fa fa-times" style="color:red"></i>'
  var filterVal = 0
  if (successful) {
    icon = '<i class="fa fa-check" style="color:green"></i>'
    filterVal = 1
  }

  var args = [entity, group, key]
  var argsStr = "'" + args.join("','") + "'"

  return (
    '<button class="btn btn-sm filter-'+key+'"' +
      ' onclick="CCF.pcp.showEditableInfo('+argsStr+')">' + icon + ' ' +
      ' <i class="hidden sorting filtering '+key+'">'+filterVal+'</i>' +
    '</button>'
  )
}

CCF.pcp.showStatusInfo = function(entity, group) {

  CCF.pcp.getStatusEntity(entity, group)
  var modalTitle = CCF.pcp.selectedEntity["entityLabel"] + " status"
  var submittedDateStr = ""
  var updatedDateStr = ""

  if (CCF.pcp.selectedEntity.statusTime) {
    updatedDateStr = updatedDateStr += new Date(CCF.pcp.selectedEntity.statusTime)
  }
  if (CCF.pcp.selectedEntity.submitTime) {
    submittedDateStr = "<b>Submitted on </b> "
    submittedDateStr += new Date(CCF.pcp.selectedEntity.submitTime)
  }

  var content = '<div>' + CCF.pcp.selectedEntity.statusInfo + '</div>' +
    '<div>' + submittedDateStr + '</div>'
    // '<div><p>' + updatedDateStr + '</div>'

  XNAT.ui.dialog.open({
    title: modalTitle,
    // width: '600px',
    // height: '400px',
    content: content,
    footerContent: updatedDateStr,
    buttons: [{
      label: 'OK',
      default: true,
      close: true
    }]
  });
}

CCF.pcp.showEntityInfo = function(entity, group, key) {

  CCF.pcp.getStatusEntity(entity, group)
  var modalTitle = CCF.pcp.selectedEntity["entityLabel"] + " " + key + " info"
  var dateStr = ""
  if (CCF.pcp.selectedEntity[key + 'Time']) {
    dateStr = new Date(CCF.pcp.selectedEntity[key + 'Time'])
  }

  // var content = '<div>' + CCF.pcp.selectedEntity[key + 'Info'] + '</div><hr>' +
  //   '<div>' + dateStr + '</div>'
  var content = '<div>' + CCF.pcp.selectedEntity[key + 'Info'] + '</div>'

  XNAT.ui.dialog.open({
    title: modalTitle,
    content: content,
    footer: dateStr,
    buttons: [{
      label: 'OK',
      default: true
    }]
  });
}

CCF.pcp.showEditableInfo = function(entity, group, key) {

  CCF.pcp.getStatusEntity(entity, group)

  var modalTitle = CCF.pcp.selectedEntity["entityLabel"] + " " + key + " info"
  var dateStr = ""
  if (CCF.pcp.selectedEntity[key + 'Time']) {
    dateStr = new Date(CCF.pcp.selectedEntity[key + 'Time'])
  }

  XNAT.ui.dialog.open({
    title: modalTitle,
    esc: true,
    content: '<div id="pcp-editable-info"></div>',
    footerContent: dateStr,
    beforeShow: function(obj) {
      var container = obj.$modal.find('#pcp-editable-info')
      renderInfoPanel(container)
    },
    buttons: [
    {
      label: "Save",
      default: true,
      action: function() {
        if ($('#'+key).val() === "false") {
          CCF.pcp.selectedEntity[key] = false
        } else {
          CCF.pcp.selectedEntity[key] = true
        }
        CCF.pcp.selectedEntity[key+'Info'] = $('#'+key+'Info').val()
        // CCF.pcp.selectedEntity[key + 'Time'] = Number(new Date())
        CCF.pcp.updateStatusEntity()
        CCF.pcp.renderPipelineTable()
      }
    },
    {
      label: 'Cancel',
      default: false,
      close: true
    }]
  });

  function renderInfoPanel(container) {
    var dropdown =
    '<div class="panel-element" style="display:inline-block">' +
      '<label class="element-label" style="display:inline-block">' + key + '?</label>' +
      '<div class="element-wrapper">' +
        '<select id="'+key+'" title="'+key+'">' +
          '<option value="true">Yes</option>' +
          '<option value="false">No</option>' +
        '</select>' +
      '</div>' +
    '</div>'
    $(container).append(dropdown)
    $('#'+key).val(CCF.pcp.selectedEntity[key].toString())

    // var $textArea = $('<br><br><textarea id="'+key+'Info" rows=8 cols=75/>')
    var $textArea = $('<br><br><textarea id="'+key+'Info" rows=12 style="min-height:120px; height:100%; width:100%"/>')
    $textArea.text(CCF.pcp.selectedEntity[key+'Info']);
    $(container).append($textArea)
  }
}

CCF.pcp.showNotes = function(entity, group) {
  CCF.pcp.getStatusEntity(entity, group)

  var tempDiv = spawn('div', CCF.pcp.selectedEntity.notes);

  var editorConfig = {
      language: 'text'
  }

  var editor = XNAT.app.codeEditor.init(tempDiv, editorConfig);

  editor.openEditor({
      title: 'Notes for ' + CCF.pcp.selectedEntity.entityLabel + ' ' + CCF.pcp.selectedEntity.subGroup,
      id: "pcp-notes-modal",
      esc: true,
      buttons: {
        save: {
          label: 'Save',
          isDefault: true,
          action: function(modal) {
            CCF.pcp.selectedEntity.notes = editor.getValue().code;
            CCF.pcp.updateStatusEntity()
            CCF.pcp.renderPipelineTable()
            modal.close()
          }
        },
        cancel: {
          label: 'Cancel',
          action: function(modal){
            modal.close()
          }
        }
      }
      // other xmodal properties
  });
}

CCF.pcp.renderStatusUpdate = function() {

  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities.length < 1) {
     XNAT.ui.banner.top(3000, "Please select at least one row to update.")
    return
  } else {

     xmodal.confirm({
        height: 220,
        scroll: false,
        content: "" +
          "<p>Run status update for " + CCF.pcp.submissionEntities.length + " rows?</p>",
        okAction: function(){
          var submitUrl = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/pipeline/' + CCF.pcp.pipeline + '/updateStatusEntities'
          //console.log(JSON.stringify(submitJson))

          XNAT.ui.banner.top(4000, "Processing status update.  The table will reload when processing completes. ");
          $.ajax({
            type: "POST",
            url: submitUrl,
            cache: false,
            async: true,
            contentType: "application/json; charset=utf-8",
            context: this,
            data: JSON.stringify(CCF.pcp.submissionEntities),
            dataType: 'json'
          }).done( function(data, textStatus, jqXHR) {
            var msg;
            if (jqXHR.status == 202) {
               msg = "<b>The update process has returned early because it is expected to take approximately " + jqXHR.responseText + " seconds to run. " +
                     " Only some of the selected rows have been updated.</b><br><br>The process will keep running and you may refresh or check this page again " +
                     " later to see the final results.";
               xmodal.message({ title: "Incomplete",
				width: '500px',
				height: '300px',
				content: msg
			});
	    } else {
               msg = "Status has been updated for all selected rows";
               XNAT.ui.banner.top(3000, msg);
            }
            CCF.pcp.renderPipelineTable(true);
          }).fail( function(data, textStatus, jqXHR) {
            $(".top-banner").hide();
            xmodal.message("Processing Error","ERROR: Could not process the update.  This is likely because an update process is currently running for this project.");
            CCF.pcp.renderPipelineTable(true)
          });
	},
        cancelAction: function(){
          XNAT.ui.banner.top(3000, "Update operation cancelled.");
          CCF.pcp.renderPipelineTable(true);
	},
     });

  }

}

























CCF.pcp.renderSubmitParameters = function() {
  var url = "/xapi/pipelineControlPanel/project/" + CCF.pcp.project +
    "/pipeline/" + CCF.pcp.pipeline + "/submitParametersYaml"

  CCF.pcp.getSubmissionEntities()
  // Make sure at least one entity is selected before rendering modal
  if (CCF.pcp.submissionEntities < 1) {
    XNAT.ui.banner.top(3000, "Please select at least one entity before launching")
    return
  }

  XNAT.ui.dialog.open({
    title: CCF.pcp.pipeline + " parameters",
    content:  '<div class="panel">' +
                '<div id="pcp-parameters"></div><hr>' +
                '<div id="pcp-parameters-message"></div>' +
              '</div>',
    beforeShow: function(obj) {
      XNAT.xhr.get({
        url: XNAT.url.rootUrl(url),
        success: function (data) {
          if (typeof data[0] === 'string') {
            var yml = YAML.parse(data[0]);
          }
          if (yml) {
            XNAT.spawner.spawn(yml).render('#pcp-parameters');
          } else {
            // clear the loading div
          }
          $('#pcp-parameters-message').append(CCF.pcp.submissionEntities.length +
            " entities selected for launch")
        },
        fail: function () {
          console.log("Failed to get pipeline parameters Yaml")
        }
      });

    },
    buttons: [
    {
        label: 'Launch',
        isDefault: true,
        close: true,
        action: function() {
          // CCF.pcp.getSubmissionParams(this)
          CCF.pcp.submissionParams = {}

          $("#pcp-parameters :input").each(function() {
            if (this.id) {
              CCF.pcp.submissionParams[this.id] = this.value
            }
          })

          var submitUrl = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
          '/pipeline/' + CCF.pcp.pipeline + '/pipelineSubmit'
          var submitJson = {
            'entities': CCF.pcp.submissionEntities,
            'parameters': CCF.pcp.submissionParams
          }
          //console.log(JSON.stringify(submitJson))

          $.ajax({
            type: "POST",
            url: submitUrl,
            cache: false,
            async: true,
            contentType: "application/json; charset=utf-8",
            context: this,
            data: JSON.stringify(submitJson),
            dataType: 'json'
          }).done( function(data, textStatus, jqXHR) {
            var msg = CCF.pcp.pipeline + ' submitted for ' + CCF.pcp.submissionEntities.length + ' entities'
            XNAT.ui.banner.top(2000, msg);
            CCF.pcp.renderPipelineTable(true)
          }).fail( function(data, textStatus, jqXHR) {
            XNAT.ui.dialog.alert("ERROR: Pipeline launch failed (STATUS=" + textStatus + ").");
          });
        }
      },
			{
        label: 'Cancel',
        isDefault: false,
        close: true
      }
    ]
  })
}

CCF.pcp.getSubmissionEntities = function() {
  CCF.pcp.submissionEntities = []

  var $checkedEntities = document.querySelectorAll('input[name=entity-check]:checked')
  var entityLabels = []

  $checkedEntities.forEach(function(entity) {
    entityLabels.push(entity.id)
  })

  // Build entities list to submit to PCP
  for (var i = 0; i < entityLabels.length; i++) {
    var entity = entityLabels[i].split('-')[0]

    for (var j = 0; j < CCF.pcp.allStatusEntities.length; j++) {
      // console.log(entity)
      // console.log(CCF.pcp.allStatusEntities[j].entityLabel)
      if (entity === CCF.pcp.allStatusEntities[j].entityLabel) {
        CCF.pcp.submissionEntities.push(CCF.pcp.allStatusEntities[j])
      }
    }
  }
}

CCF.pcp.getStatusEntity = function(entity, group) {
  var url = "/xapi/pipelineControlPanel/project/" + CCF.pcp.project +
    "/pipeline/" + CCF.pcp.pipeline +
    "/entity/" + entity +
    "/group/" + group + "?cached=true"

  $.ajax({
    type: "GET",
    url:  url,
    cache: false,
    async: false,
    context: this,
    dataType: 'json'
  }).success( function(data, textStatus, jqXHR) {
    CCF.pcp.selectedEntity = data
  }).fail( function(data, textStatus, jqXHR) {
    XNAT.ui.dialog.alert(textStatus + ": Could not get entity info for " + entity);
    CCF.pcp.selectedEntity = {}
  });
}

CCF.pcp.getAllStatusEntities = function() {
  var url = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
    '/pipeline/' + CCF.pcp.pipeline +
    '/status?cached=false&condensed=false'

  $.ajax({
    type: "GET",
    url:  url,
    cache: false,
    async: true,
    context: this,
    dataType: 'json'
  }).success( function(data, textStatus, jqXHR) {
    CCF.pcp.allStatusEntities = data
    console.log('done getting all entities')
    // XNAT.ui.dialog.loading.closeAll()
  }).fail( function(data, textStatus, jqXHR) {
    XNAT.ui.dialog.alert(textStatus + ": Could not get entity info");
    CCF.pcp.allStatusEntities = []
    // XNAT.ui.dialog.loading.closeAll()
  });
}

CCF.pcp.updateStatusEntity = function() {
  var url = '/xapi/pipelineControlPanel/project/' + CCF.pcp.project +
    '/pipeline/' + CCF.pcp.pipeline + '/updateStatusEntity'

  $.ajax({
    type: "POST",
    url: url,
    cache: false,
    async: true,
    contentType: "application/json; charset=utf-8",
    context: this,
    data: JSON.stringify(CCF.pcp.selectedEntity),
    dataType: 'json'
  }).success( function(data, textStatus, jqXHR) {
    XNAT.ui.banner.top(2000, CCF.pcp.selectedEntity.entityLabel + ' entity updated successfully');
  }).fail( function(data, textStatus, jqXHR) {
    XNAT.ui.dialog.alert(textStatus + ": " + CCF.pcp.selectedEntity.entityLabel + " not updated");
  });
}
